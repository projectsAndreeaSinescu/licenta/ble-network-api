'use strict';

var loopback = require('loopback');
var boot = require('loopback-boot');

var app = module.exports = loopback();
var LoopBackContext = require('loopback-context');
loopback.env=process.env.NODE_ENV || 'development';

var cluster = require('cluster');
var numCPUs = process.env.NODE_CPU ? process.env.NODE_CPU : require('os').cpus().length;

if (numCPUs == 1)
{
    app.start = function () {
        return app.listen(function () {
            app.emit('started');
            var baseUrl = app.get('url').replace(/\/$/, '');
            console.log('Web server listening at: %s', baseUrl);
            if (app.get('loopback-component-explorer')) {
                var explorerPath = app.get('loopback-component-explorer').mountPath;
                console.log('Browse your REST API at %s%s', baseUrl, explorerPath);
            }
        });
    };
}
else
{
    app.start = function () {
        // start the web server
        if (cluster.isMaster) {
            console.log("Master is running" + process.pid);

            // Fork workers.
            for (var i = 0; i < numCPUs; i++) {
                cluster.fork();
            }

            cluster.on('exit', function (worker, code, signal) {
                console.log("worker died" + worker.process.pid);
            });
        } else {
            // Workers can share any TCP connection
            // In this case it is an HTTP server
            return app.listen(function () {
                app.emit('started');
                var baseUrl = app.get('url').replace(/\/$/, '');
                console.log('Web server listening at: %s', baseUrl);
                if (app.get('loopback-component-explorer')) {
                    var explorerPath = app.get('loopback-component-explorer').mountPath;
                    console.log('Browse your REST API at %s%s', baseUrl, explorerPath);
                }
            });

        }
    };
}

// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot(app, __dirname, function (err) {
    if (err) throw err;

    // start the server if `$ node server.js`
    if (require.main === module)
        app.start();
});

var path = require('path');
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));

app.use(LoopBackContext.perRequest());
app.use(loopback.token());
app.use(function setCurrentUser(req, res, next) {
    if (!req.accessToken) {
        return next();
    }
    app.models.clientusers.findById(req.accessToken.userId, function(err, user) {
        if (err) {
            return next(err);
        }
        if (!user) {
            return next(new Error('No user with this access token was found.'));
        }
        var loopbackContext = LoopBackContext.getCurrentContext();
        if (loopbackContext) {
            loopbackContext.set('currentToken', req.accessToken);
            loopbackContext.set('currentUser', user);
        }
        next();
    });
});

require("./routes")(app);

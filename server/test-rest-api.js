var server = require('./server');
var async = require('async');

var http = require(process.env.TEST_SITE_URL || "http");
var hostURL = process.env.TEST_SITE_URL || "127.0.0.1";
var version = process.env.TEST_VERSION || "v1";
var accessToken = process.env.TEST_TOKEN || "PLdZ7y7PJtRIvqJyxC4YjSgmvPFBREXRnUupF8A72cKnqUS9mEOLxjsMNJ4DldKg";

function getRequests()
{
    var requests = [];

    for (var model in server.models)
    {
        if (server.models[model].sharedClass)
        {
            var remotes = server.models[model].sharedClass._methods;
            for (var i = 0; i < remotes.length; i++)
            {
                // var params = remotes[i].accepts;
                // var queryParams = [];
                // for (var pIndex = 0; pIndex < params.length; pIndex ++)
                // {
                //     if (params[pIndex].http.source == "query")
                //     {
                //
                //     }
                // }
                if (server.models[model].hsDisabled && (remotes[i].http.verb != "all") && (remotes[i].http.verb != "del"))
                {
                    
                    if (remotes[i].tests)
                    {
                        for (var tIndex = 0; tIndex < remotes[i].tests.length; tIndex++)
                        {
                            var params = remotes[i].accepts;
                            var queryParams = [];
                            var body;
                            for (var paramIndex = 0; paramIndex < params.length; paramIndex++)
                            {
                                if (params[paramIndex].http.source == "query")
                                {
                                    queryParams.push(params[paramIndex].arg +"="+remotes[i].tests[tIndex][params[paramIndex].arg])
                                }
                                else
                                {
                                    body = remotes[i].tests[tIndex][params[paramIndex].arg];
                                }
                            }
                            
                            var request = {
                                host: hostURL,
                                path: "/api/" +version + server.models[model].sharedClass.http.path + remotes[i].http.path+"?access_token="+accessToken +queryParams.join("&"),
                                method: remotes[i].http.verb,
                                headers: {
                                    'Content-Type': 'application/json'
                                },
                                timeout: 15000
                            };
                            request.specificTest = true;
                            request.body = body;
                            
                            if (process.env.NODE_PORT)
                            {
                                request.port = process.env.NODE_PORT;
                            }
                            else if(require("./config.json").port)
                            {
                                request.port = require("./config.json").port;
                            }
                            requests.push(request);
                        }
                    }
                    if ((server.models[model].hsDisabled.indexOf(remotes[i].name) == -1) &&
                        (remotes[i].name.toLowerCase().indexOf("logout") == -1)
                        && (remotes[i].name.toLowerCase().indexOf("removeaccount") == -1))
                    {
                        var params = remotes[i].accepts;
                        var skip = false;
                        for (var paramIndex = 0; paramIndex < params.length; paramIndex++)
                        {
                            if (params[paramIndex].arg == "req")
                            {
                                skip = true;
                            }
                        }
                        if (!skip)
                        {
                            var request = {
                                host: hostURL,
                                path: "/api/" +version + server.models[model].sharedClass.http.path + remotes[i].http.path+"?access_token="+accessToken,
                                method: remotes[i].http.verb,
                                headers: {
                                    'Content-Type': 'application/json'
                                },
                                timeout: 15000
                            };
                            if (process.env.NODE_PORT)
                            {
                                request.port = process.env.NODE_PORT;
                            }
                            else if(require("./config.json").port)
                            {
                                request.port = require("./config.json").port;
                            }
                            requests.push(request);
                        }
                        
                    }
                }
                
            }
        }
    }
    
    return requests;
}


var requests = getRequests();
async.eachSeries(requests,
    function iteratee(request, callback){
    try {
        var post_request = http.request(request, function(res) {
            var body = '';
        
            res.on('data', function(chunk)  {
                body += chunk;
            });
        
            res.on('end', function() {
            
                try {
    
                    if (typeof body == 'string') {
                        body = JSON.parse(body);
                    }
                    if (body.error) {
                        if (body.error.statusCode) {
                            console.log("Status CODE - " + body.error.statusCode + " - " + request.path, JSON.stringify(body));
                            return callback();
            
                        }
                    }
                    if (request.specificTest)
                    {
                        if (!body.error_code) {
                            console.log("UNKNOWN - " + request.path, JSON.stringify(body));
                            return callback();
        
                        }
                        var errorCode = body.error_code.code;
                        if (errorCode != 0) {
                            console.log("ERROR - " + request.path, JSON.stringify(body));
                            callback();
                        }
                        else {
                            console.log("COMPLETED - " + request.path, JSON.stringify(body));
                            callback();
                        }
                        
                    }
                    else {
                        console.log("COMPLETED - " + request.path, JSON.stringify(body));
                        callback();
                    }
                }

                catch (e) {
                    console.log("ERROR - "+requests.path,e.stack);
                    callback();
    
                }
            
            });
        
            res.on('error', function(e) {
                console.log("ERROR - "+request.path,e.stack);
                callback();
            });
        });
    
        post_request.on('error', function(e) {
            console.log("ERROR - "+request.path,e.stack);
            callback();
        });
    
    
        if (request.body)
        {
            console.log("START - " + request.path, request.body);
            post_request.write(JSON.stringify(request.body), "utf8");
        }
        else
        {
            console.log("START - " + request.path);
        }
        post_request.end();
    }
    catch (e) {
        console.log("ERROR - "+requests.path,e.stack);
        callback();
    
    }

    }, function done(){
        console.log("COMPLETED TEST");
    });
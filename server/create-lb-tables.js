var server = require('./server');
var ds = server.dataSources.mysql;
var lbTables = ['AccessToken', 'ACL', 'RoleMapping', 'Role', 'clientusers', 'applicationParams',
'serverNotification', 'messages'];

var async = require('async');
var generalError = undefined;

async.eachSeries(lbTables, function iteratee(table, callback) {
    if (!generalError)
    {
        ds.autoupdate([table], function (err) {
            if (err)
            {
                generalError = err;
            }
            callback();

        });

    }
    else
    {
        callback();
    }
}, function done() {
    if (generalError)
    {
        ds.disconnect();
        throw generalError;
    }
    else {
        require("fs").readFile("./DatabaseSQL.sql", "utf8", function(err, data){
            if (err) throw err;
            var sql = data;

            ds.connector.query(sql, function(err){
                if (err) throw err;
                console.log('Loopback tables [' + lbTables + '] created in ', ds.adapter.name);
                ds.disconnect();
            });

        });
    }
});


//
// ds.automigrate(lbTables, function(er) {
//     if (er) throw er;
//    console.log('Loopback tables [' + lbTables + '] created in ', ds.adapter.name);
//     ds.disconnect();
// });


// node server/create-lb-tables.js  to run

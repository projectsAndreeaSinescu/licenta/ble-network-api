

var LocationManagement = {};

//helpers

function deg2rad(deg) {
    return deg * (Math.PI/180)
}

//exported
LocationManagement.validateCoordinates = function(latitude, longitude)
{
    if ((latitude == undefined) || (longitude == undefined)) return false;

    if (typeof latitude != "number") return false;
    if (typeof longitude != "number") return false;

    if ((latitude < -90) || (latitude > 90)) return false;
    if ((longitude < -180) || (longitude > 180)) return false;

    return true;
};

LocationManagement.validateCoordinateRadius = function(latitude, longitude, radius)
{
    if (radius == undefined) return false;

    if (!LocationManagement.validateCoordinates(latitude, longitude)) return false;
    if (radius < 0) return false;

    return true;
};

///distance between 2 coordinates in meters
LocationManagement.distanceMeters = function (sourceLatitude,sourceLongitude,destinationLatitude,destinationLongitude) {
    var R = 6371000; // Radius of the earth in m
    var dLat = deg2rad(destinationLatitude - sourceLatitude);  // deg2rad below
    var dLon = deg2rad(destinationLongitude - sourceLongitude);
    var a =
        Math.sin(dLat/2) * Math.sin(dLat/2) +
        Math.cos(deg2rad(sourceLatitude)) * Math.cos(deg2rad(destinationLatitude)) *
        Math.sin(dLon/2) * Math.sin(dLon/2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c; // Distance in m
    return d;
};


module.exports = LocationManagement;

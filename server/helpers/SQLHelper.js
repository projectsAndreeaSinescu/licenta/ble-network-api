var SQLHelper = {};
var async = require('async');
var promise = require('bluebird');

SQLHelper.listAllColumns = function(filename, prefix)
{
    var json = require('../models/'+filename+'.json');
    var modelName = json.name;
    var properties = json.properties;
    var columns = [];
    for (var key in properties) {
        if (properties.hasOwnProperty(key)) {
            if (!prefix)
            {
                columns.push( modelName +'.' +key +' AS ' + modelName +'_'+key);
            }
            else
            {
                columns.push( modelName +'.' +key +' AS ' + prefix +'_'+key);
            }
        }
    }
    return columns;

};


SQLHelper.filterProperties = function(table, data)
{
    var object = {};
    for (var key in data) {
        if (data.hasOwnProperty(key)) {
            if (key.startsWith(table))
            {
                object[key.replace(table+'_', '')] = data[key];
            }
        }
    }
    return object;
};


/**
 * Streams query results in CSV format, adds pagination. Replaces , with %2C in results
 * @param connector - database connector
 * @param sql - query to run
 * @param params - list of parameters for the query
 * @param columns - list of columns (if not given they will be set from the first result)
 * @param stream - output stream, to write data to
 */
SQLHelper.csvexport = function(connector, sql, params, columns, stream) {

    return new promise(function (resolve, reject) {
        var moreItems = true;
        var limit = 200;
        var offset = 0;
        var generalError = undefined;

        if (columns && !columns.length)
        {
            stream.write(columns.join(",") + "\n");
        }

        async.whilst(
            function () {
                return moreItems;
            },
            function (callback) {
                var extendedSQL = sql + " LIMIT " + limit + " OFFSET " + offset;
                connector.query(extendedSQL, params, function(error, results){
                    if (error)
                    {
                        console.error("csvexport", err);
                        generalError = require('../../settings.json').response.db_error;
                        return callback();
                    }
                    if (results.length == 0)
                    {
                        moreItems = false;
                        return callback();
                    }
                    for (var index = 0; index < results.length; index++)
                    {
                        var values = [];
                        if (!columns || !columns.length)
                        {
                            //assume first line has all columns
                            columns = Object.keys(results[index]);
                            stream.write(columns.join(",") + "\n");
                        }
                        for(var columnIndex = 0; columnIndex < columns.length; columnIndex++)
                        {
                            if (results[index][columns[columnIndex]])
                            {
                                values.push(results[index][columns[columnIndex]]).replace(/,/g, "%2C");
                            }
                            else
                            {
                                values.push("");
                            }
                        }
                        stream.write(values.join(",") + "\n");
                    }
                    offset += limit;
                    return callback()
                });
            },
            function (err) {
                if (err)
                {
                    console.error("csvexport", err);
                    generalError = require('../../settings.json').response.db_error;
                }
                if (generalError)
                {
                    reject(generalError);
                }
                else
                {
                    resolve();
                }
            }
        );
    });

};




module.exports = SQLHelper;

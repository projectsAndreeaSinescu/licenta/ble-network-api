var promise = require('bluebird');
var settings = require("../../settings.json");
var admin = require("firebase-admin");

//TODO: Replace with actual firebase key
// var firebaseKey = require("./startupproject-17c21-firebase-adminsdk-bmwwq-9099810a02.json");
var firebaseKey = require("./blenetwork-e7b83-firebase-adminsdk-5bcey-4e4d982f2e.json");

admin.initializeApp({
	credential: admin.credential.cert(firebaseKey)
});
var notificationTTL = 24 * 3600;
var NotificationManager = {};

/**
 * Generate a localized payload
 * @param title? {string?}
 * @param message? {string?}
 * @param titleLocKey? {string?}
 * @param titleLocArgs? {[string]?}
 * @param bodyLocKey? {[string?}
 * @param bodyLocArgs? {[string]?}
 * @param additionalData? {{string:*}?}
 * @return {{titleLocKey: string, titleLocArgs: string, bodyLocKey: string, bodyLocArgs: string, key: *}}
 */
NotificationManager.generatePayload = function (title, body, titleLocKey, titleLocArgs, bodyLocKey, bodyLocArgs, additionalData)
{
	var payload = {
	};
	if (title) payload.title = title;
	if (body) payload.body = body;
	if (titleLocKey) payload.titleLocKey = titleLocKey;
	if (titleLocArgs) payload.titleLocArgs = titleLocArgs;
	if (bodyLocKey) payload.bodyLocKey = bodyLocKey;
	if (bodyLocArgs) payload.bodyLocArgs = bodyLocArgs;

	if (additionalData)
	{
		Object.keys(additionalData).forEach(function (key) {
			payload[key] = "" + additionalData[key];
		});

	}

	return payload;
};



//tag?: string;
//     body?: string;
//     icon?: string;
//     badge?: string;
//     color?: string;
//     sound?: string;
//     title?: string;
//     bodyLocKey?: string;
//     bodyLocArgs?: string;
//     clickAction?: string;
//     titleLocKey?: string;
//     titleLocArgs?: string;
//     [key: string]: string | undefined;
NotificationManager.send = function (data, pushToken, silent) {
	return new promise(function (resolve, reject) {
		var pushMessage = {
			apns : {
				content_available: true,
				time_to_live: notificationTTL
			},
			data: data,
			token: pushToken // required
		};

		if (!silent)
		{
			pushMessage.notification = data;
		}

		admin.messaging().send(pushMessage).then(function(response){
			resolve(response);
		}).catch(function(error){
			console.log('error - push-errors', error);
			reject(settings.response.firebase_general_error);
		});
	});
};

NotificationManager.sendToTopic = function (data, topic, silent) {
	return new promise(function (resolve, reject) {
		var pushMessage = {
			data: data,
			topic: "/topics/" + topic,
		};
		if (data.titleLocKey || data.bodyLocKey)
		{
			pushMessage.apns = {
				payload: {
					aps: {
						alert: {
						}
					}
				}
			};

			var alert = pushMessage.apns.payload.aps.alert;
			alert.titleLocKey = data.titleLocKey;
			alert.titleLocArgs = data.titleLocArgs;
			alert.locKey = data.bodyLocKey;
			alert.locArgs = data.bodyLocArgs;

			if (data.titleLocArgs)
            {
                data.titleLocArgs = JSON.stringify(data.titleLocArgs);

            }
			if (data.bodyLocArgs)
            {
                data.bodyLocArgs = JSON.stringify(data.bodyLocArgs);
            }
		}

		if (!silent)
		{
			pushMessage.notification = {title: data.title, body: data.body};
		}

		admin.messaging().send(pushMessage).then(function(response){
			resolve(response);
		}).catch(function(error){
			console.log('error - push-errors', error);
			reject(settings.response.firebase_general_error);
		});

	});

};

module.exports = NotificationManager;

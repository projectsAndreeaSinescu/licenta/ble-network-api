var settings = require('../../settings.json');
var DevelopmentManagement = {};
var loopback = require('loopback');

DevelopmentManagement.reportErrors = function(subject, body){
    try {
        var app = require('../../server/server');
        app.models.Email.send({
            to: settings.internalDevelopment.debugEmail,
            subject: settings.hostname[loopback.env ? loopback.env : "development"] + ": " + subject + " - " + new Date(),
            text: body
        }, function(err) {
            if (err) return console.log('> error sending email',err.stack);
        });
    }
    catch (e) {
        console.log("DevelopmentManagement.reportErrors", e);
    }
};

module.exports = DevelopmentManagement;

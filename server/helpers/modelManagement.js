var promise = require('bluebird');
var async = require('async');
var settings = require("../../settings.json");
var app = require('../../server/server');
var LoopBackContext = require('loopback-context');
var loopback = require('loopback');

var ModelManagement = {};
/**
 * Updates existing items and inserts new ones, in the event of an error, a rejected(settings.response). Will resolve with the updated items
 * @param model {Model} - database model
 * @param objects {Array} - items that will be upserted
 * @return {promise}
 */
ModelManagement.bulkUpsert = function(model, objects) {

    return new promise(function(resolve, reject){
        var generalError = undefined;
        var updatedObjects = [];
        async.eachSeries(objects, function iteratee(object, callback) {
            if (!generalError)
            {

                model.upsert(object).then(function(updatedObject){
                    updatedObjects.push(updatedObject);
                    callback();
                }).catch(function (error) {
                    generalError = error;
                    callback();
                });
            }
            else
            {
                callback();
            }
        }, function done() {
            if (generalError)
            {
                console.log("ModelManagement.bulkUpsert(" + model + ")",generalError.stack);
                if (updatedObjects.length == 0)
                {
                    reject(settings.response.db_error);
                }
                else
                {
                    reject(settings.response.db_error);
                }
            }
            else
            {
                resolve(updatedObjects);
            }
        });
    });

};

/**
 * Checks if an item exists, based on a key value
 * @param item {model} - item that has to be checked
 * @param array {array} - array that has to be checked
 * @param key {string}? - value that must be compared - defaults to id
 * @return {model} - the object that has the same value for key
 */
ModelManagement.arrayFind = function(item, array, key) {
    if (!item[key])
    {
        return undefined;
    }
    return ModelManagement.arrayFindByValue(item.id, array, key);
};

/**
 * Checks if an item exists, based on a key value
 * @param array {array} - array that has to be checked
 * @param key {string}? - value that must be compared - defaults to id
 * @return {model} - the object that has the same value for key
 */
ModelManagement.arrayFindByValue = function(keyValue, array, key) {
    if (!key)
    {
        key = "id";
    }
    if (!keyValue)
    {
        return undefined;
    }

    var existingItem = array.find(function(element) {
        return element[key] == keyValue;
    });

    return existingItem;
};

/**
 * Wrap responses:
 *  - success
 *  - errors
 *  - 401 responses is left as it is, rest of the responses are wrapped to return error_code and optional result
 *  - for error responses in development environment - the error is also returned
 * @param Model
 */
ModelManagement.responseWrapper = function(Model)
{
    Model.afterRemote('*', function(context, modelInstance, next) {
        var result = context.result;
        if (result && result.error_code)
        {
            next();
        }
        else
        {
            //add wrapper
            var response = {
                error_code : settings.response.ok,
                response : result
            };
            context.result = response;
            next();
        }

    });

    Model.afterRemoteError('*', function(context, next) {
        if (context.error.statusCode == 401)
        {
            next();
        }
        else if(context.error.statusCode == 400)
        {
            var result = {};
            result.error_code = settings.response.missing_params;
            if (loopback.env == "development")
            {
                console.log(typeof  context.error);
                result.error_details = {message: context.error.message, stack: context.error.stack.substr(0, 500)};
            }
            context.res.status(200).send(result);
            next();
        }
        else
        {
            var result = {};
            if (context.error.sqlState)
            {
                result.error_code = settings.response.db_error;
            }
            else
            {
                result.error_code = settings.response.general_error;
            }
            if (loopback.env == "development")
            {
                console.log(typeof  context.error);
                if (context.error.stack)
                {
                    result.error_details = {message: context.error.message, stack: context.error.stack.substr(0, 500)};
                }
            }
            context.res.status(200).send(result);
            next();

        }
    });
};
/**
 * Restricts access to specific methods to the a given IPRange.
 * @param Model
 * @param remotes {String[]}
 * @param ipRanges {String[]}
 */
ModelManagement.restrictAccessToIPRanges = function(Model, remotes, ipRanges)
{
    for (var index = 0; index < remotes.length; index++)
    {
        Model.beforeRemote(remotes[index], function(context, unused, next) {
            var ipRangeCheck = require("ip-range-check");
            console.log(context.req.headers);
            var callerIPPath = context.req.headers["x-forwarded-for"];
            if (!callerIPPath)
            {
                next();
            }
            else
            {
                var callerIP = callerIPPath.split(",")[0].replace(" ", "");
                if (ipRangeCheck(callerIP, ipRanges))
                {
                    next();
                }
                else
                {
                    var error = new Error();
                    error.statusCode = 401;
                    error.message ='Authorization Required';
                    if (loopback.env == 'development')
                    {
                        error.message +=  "IP restriction: path("+callerIPPath+") not allowed to " + context.method.stringName;
                    }
                    next(error);
                    return;
                }

            }
        });

    }

};

/**
 * Restricts access to specific methods to the admin. Admin status is based on Clientusers.isAdmin()
 * @param Model
 * @param remotes {String[]}
 */
ModelManagement.restrictAccessAdmin = function(Model, remotes)
{
    for (var index = 0; index < remotes.length; index++) {
        Model.beforeRemote(remotes[index], function (context, unused, next) {
            var ctx = LoopBackContext.getCurrentContext();
            var currentUser = ctx && ctx.get('currentUser');

            if (!currentUser) {
                var error = new Error();
                error.statusCode = 401;
                error.message = 'Authorization Required';
                next(error);
                return;
            }
            var Clientusers = app.models.Clientusers;
            if (!Clientusers.isAdmin(currentUser)) {
                var error = new Error();
                error.statusCode = 401;
                error.message = 'Authorization Required';
                next(error);
                return;
            }

            next();
        });
    }
};

/**
 * Restricts access to specific methods to the authenticated user
 * @param Model
 * @param remotes {String[]}
 */
ModelManagement.restrictAccessAuthenticated = function(Model, remotes)
{
    for (var index = 0; index < remotes.length; index++) {
        Model.beforeRemote(remotes[index], function (context, unused, next) {
            var ctx = LoopBackContext.getCurrentContext();
            var currentUser = ctx && ctx.get('currentUser');

            if (!currentUser) {
                var error = new Error();
                error.statusCode = 401;
                error.message = 'Authorization Required';
                next(error);
                return;
            }

            next();
        });
    }
};

/**
 * Restricts access to specific methods to specific roles
 * @param Model
 * @param remotes {String[]}
 * @param roles {String[]}
 */
ModelManagement.restrictAccessRole = function(Model, remotes, roles)
{
    for (var index = 0; index < remotes.length; index++) {
        Model.beforeRemote(remotes[index], function (context, unused, next) {
            var ctx = LoopBackContext.getCurrentContext();
            var currentUser = ctx && ctx.get('currentUser');

            if (!currentUser) {
                var error = new Error();
                error.statusCode = 401;
                error.message = 'Authorization Required';
                next(error);
                return;
            }

            if (roles.indexOf(currentUser.role) != -1) {
                next();
            }
            else
            {
                var error = new Error();
                error.statusCode = 401;
                error.message = 'Authorization Required';
                next(error);
                return;
            }
        });
    }
};

/**
 * Adds a method to export csv file, only for SQL
 * @param Model - model from which to call
 * @param tableName - name of the table
 */
ModelManagement.addCSVExport = function (Model, tableName)
{
    //Sample on how to activate
    // ModelManagement.addCSVExport(Clientusers, "clientusers");

    Model.adminCSVExport = function(res, cb) {

        var datetime = new Date();
        res.set('Expires', 'Tue, 03 Jul 2001 06:00:00 GMT');
        res.set('Cache-Control', 'max-age=0, no-cache, must-revalidate, proxy-revalidate');
        res.set('Last-Modified', datetime +'GMT');
        res.set('Content-Type','application/force-download');
        res.set('Content-Type','application/octet-stream');
        res.set('Content-Type','application/download');
        res.set('Content-Disposition','attachment;filename=Data.csv');
        res.set('Content-Transfer-Encoding','binary');

        var SQLHelper = require('./SQLHelper.js');
        SQLHelper.csvexport(Model.dataSource.connector, "SELECT * FROM " + tableName, [], undefined, res).then(function (value) {

        }).catch(function () {
            console.log(err);
            res.write("Not All Items were downloaded");
        }).finally(function () {
            res.end();

        })
    };
    Model.remoteMethod(
        'adminCSVExport',
        {
            accepts: [
                {arg: 'res', type: 'object', 'http': {source: 'res'}}
            ],
            http: {path: '/adminCSVExport', verb: 'get'},
            description: 'Get all data as CSV',
            returns: [
                {"arg": "error_code", "type": "object"},
                {"arg": "response", "type": "object"}
            ]
        }
    );
    ModelManagement.restrictAccessAdmin(Model, ["adminCSVExport"]);

}

/**
 * Disable default methods, with exceptions
 * @param model
 * @param methodsToExpose  {String[]} - list of exceptions
 */
ModelManagement.disableAllMethods = function(model, methodsToExpose) {
    if (model && model.sharedClass) {
        methodsToExpose = methodsToExpose || [];
        var modelName = model.sharedClass.name;
        var methods = model.sharedClass.methods();
        var relationMethods = [];
        var hiddenMethods = [];

        try {
            Object.keys(model.definition.settings.relations).forEach(function (relation) {
                relationMethods.push({name: '__findById__' + relation, isStatic: false});
                relationMethods.push({name: '__destroyById__' + relation, isStatic: false});
                relationMethods.push({name: '__updateById__' + relation, isStatic: false});
                relationMethods.push({name: '__exists__' + relation, isStatic: false});
                relationMethods.push({name: '__link__' + relation, isStatic: false});
                relationMethods.push({name: '__get__' + relation, isStatic: false});
                relationMethods.push({name: '__create__' + relation, isStatic: false});
                relationMethods.push({name: '__update__' + relation, isStatic: false});
                relationMethods.push({name: '__destroy__' + relation, isStatic: false});
                relationMethods.push({name: '__unlink__' + relation, isStatic: false});
                relationMethods.push({name: '__count__' + relation, isStatic: false});
                relationMethods.push({name: '__delete__' + relation, isStatic: false});
            });
        } catch (err) {
        }

        var storageMethods = [];
        storageMethods.push({name: 'download', isStatic: true});
        storageMethods.push({name: 'getContainer', isStatic: true});
        storageMethods.push({name: 'getContainers', isStatic: true});
        storageMethods.push({name: 'destroyContainer', isStatic: true});
        storageMethods.push({name: 'removeFile', isStatic: true});
        storageMethods.push({name: 'getFiles', isStatic: true});
        storageMethods.push({name: 'getFile', isStatic: true});
        storageMethods.push({name: 'upload', isStatic: true});

        methods.concat(relationMethods).concat(storageMethods).forEach(function (method) {
            var methodName = method.name;
            if (methodsToExpose.indexOf(methodName) < 0) {
                hiddenMethods.push(methodName);
                model.disableRemoteMethod(methodName, method.isStatic);
                if (!model.hsDisabled)
                {
                    model.hsDisabled = [];
                }
                model.hsDisabled.push(methodName);
            }
        });

        if (hiddenMethods.length > 0) {
            console.log('\nRemote mehtods hidden for', modelName, ':', hiddenMethods.join(', '), '\n');
        }
    }
};

/**
 * Disable specific methods
 * @param model
 * @param methodsToExpose  {String[]}
 */
ModelManagement.disableSelMethods = function (model, methodsToDisable) {
    if (model && model.sharedClass) {
        methodsToDisable = methodsToDisable || [];
        var modelName = model.sharedClass.name;
        var methods = model.sharedClass.methods();
        var relationMethods = [];
        var hiddenMethods = [];

        try {
            Object.keys(model.definition.settings.relations).forEach(function (relation) {
                relationMethods.push({name: '__findById__' + relation, isStatic: false});
                relationMethods.push({name: '__destroyById__' + relation, isStatic: false});
                relationMethods.push({name: '__updateById__' + relation, isStatic: false});
                relationMethods.push({name: '__exists__' + relation, isStatic: false});
                relationMethods.push({name: '__link__' + relation, isStatic: false});
                relationMethods.push({name: '__get__' + relation, isStatic: false});
                relationMethods.push({name: '__create__' + relation, isStatic: false});
                relationMethods.push({name: '__update__' + relation, isStatic: false});
                relationMethods.push({name: '__destroy__' + relation, isStatic: false});
                relationMethods.push({name: '__unlink__' + relation, isStatic: false});
                relationMethods.push({name: '__count__' + relation, isStatic: false});
                relationMethods.push({name: '__delete__' + relation, isStatic: false});
            });
        } catch (err) {
        }

        methods.concat(relationMethods).forEach(function (method) {
            var methodName = method.name;
            for (var i = 0; i < methodsToDisable.length; i++) {
                if (methodName.indexOf(methodsToDisable[i]) >= 0) {
                    hiddenMethods.push(methodName);
                    model.disableRemoteMethod(methodName, method.isStatic);
                    if (!model.hsDisabled)
                    {
                        model.hsDisabled = [];
                    }
                    model.hsDisabled.push(methodName);
                }
            }

        });

        if (hiddenMethods.length > 0) {
            console.log('\nRemote mehtods hidden for', modelName, ':', hiddenMethods.join(', '), '\n');
        }
    }
};



module.exports = ModelManagement;

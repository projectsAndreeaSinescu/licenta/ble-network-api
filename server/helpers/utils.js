


var Utils = {};

/**
 * Formats a date to "YYYY<delimiter>MM<delimiter>DD"
 * @param date {Date} - date to format
 * @param delimiter {string?} - delimiter
 * @return {string}
 */
Utils.formatDate = function (date, delimiter) {
    var yyyy = date.getFullYear().toString();
    var mm = (date.getMonth()+1).toString();
    var dd  = date.getDate().toString();

    var mmChars = mm.split('');
    var ddChars = dd.split('');

    return yyyy + (delimiter ? delimiter : '') + (mmChars[1]?mm:"0"+mmChars[0]) + (delimiter ? delimiter : '') + (ddChars[1]?dd:"0"+ddChars[0]);
};

/**
 * Checks if a value is a number and is in the interval [minValue, maxValue]
 * @param value {number}
 * @param minValue {number?} - minimum value
 * @param maxValue {number?} - max value
 * @return {boolean}
 */
Utils.validateNumber = function(value, minValue, maxValue)
{
    if (value == undefined) return false;
    if (typeof value != "number") return false;

    if ((minValue != undefined) && (value < minValue)) return false;
    if ((maxValue != undefined) && (value > maxValue)) return false;

    return true;
};

/**
 * Get unixtime in seconds
 * @return {number} - unixtime in seconds
 */
Utils.unixtime = function()
{
    return new Date().getTime() / 1000;
};

/**
 *
 * Generate a unique token with a given prefix <env>_<prefix?>_<uuid>
 * @param prefix {string?}
 * @return {string}
 */
Utils.generateTopic = function(prefix)
{
    var loopback = require('loopback');
    var uuidV4 = require('uuid/v4');
    if (prefix)
    {
        return  loopback.env + "_" + prefix + "_" + uuidV4();
    }
    else
    {
        return  loopback.env  + "_" + uuidV4();
    }
};

/**
 * Validate if a string is an URL
 * @param url {string}
 * @return {boolean}
 */
Utils.validateURL = function(url)
{
    var ipRangeCheck = require("ip-range-check");
    var URL = require("url");
    var result = URL.parse(url);
    if (result.protocol != 'http:' && result.protocol != 'https:')
    {
        return false;
    }
    var loopback = require('loopback');
    if (loopback.env != 'developmen')
    {
        var hostname = result.hostname;
        if (ipRangeCheck(hostname, Utils.privateIPRanges))
        {
            return false;
        }
        if(hostname == "localhost")
        {
            return false;
        }
    }

    var  urlRegex = require('url-regex');

    return urlRegex({exact: true, strict: true}).test(url);

};

/**
 * Ckeck whether a string is a valid JSON
 * @param str {string}
 * @return {boolean}
 */
Utils.IsJsonString = function(str)
{
    try {
        var json = JSON.parse(str);
        return (typeof json === 'object');
    } catch (e) {
        return false;
    }
};

/**
 * Get object keys by value
 * @param obj {string}
 * @param value {string}
 * @return {array}
 */
Utils.getObjectKeysByValue = function(obj, val)
{

    var objects = [];
    for (var i in obj) {
        if (!obj.hasOwnProperty(i)) continue;
        if (typeof obj[i] == 'object') {
            objects = objects.concat(getKeys(obj[i], val));
        } else if (obj[i] == val) {
            objects.push(i);
        }
    }

    return objects;
};


Utils.extendObject = function(obj1, obj2)
{

    for (var i in obj2) {
        if (!obj1.hasOwnProperty(i)) continue;
        obj1[i] = obj2[i]
    }

    return obj1;
};


//https://en.wikipedia.org/wiki/Reserved_IP_addresses
Utils.privateIPRanges = ["0.0.0.0/8", "10.0.0.0/8", "100.64.0.0/10", "127.0.0.0/8", "169.254.0.0/16", "172.16.0.0/12",
    "192.0.0.0/24", "192.0.2.0/24", "192.88.99.0/24", "192.168.0.0/16", "198.18.0.0/15", "198.51.100.0/24", "203.0.113.0/24",
    "224.0.0.0/4", "240.0.0.0/4", "255.255.255.255/32",
    "::/0", "::/128", "::1/128", "64:ff9b::/96",
    "100::/64", "2001::/32", "2001:20::/28", "2001:db8::/32", "2002::/16", "fc00::/7", "fe80::/10", "ff00::/8", ];
//"::ffff:0:0/96", "::ffff:0:0:0/96" are allowed since they are translations from ipv4

module.exports = Utils;

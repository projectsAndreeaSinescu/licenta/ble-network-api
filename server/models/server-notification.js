'use strict';
var loopback = require('loopback');
var	LoopBackContext = require('loopback-context');
var	promise = require('bluebird');
var	async = require('async');
var Utils = require("../helpers/utils.js");
var ModelManagement = require('../helpers/modelManagement.js');
var NotificationManager = require('../helpers/notificationManager.js');

module.exports = function(ServerNotification) {

	ModelManagement.disableAllMethods(ServerNotification);
	ModelManagement.responseWrapper(ServerNotification);

	var settings = require("../../settings.json");
	var app = require('../../server/server');

	var Type = {
		internal: 0,
    bleUserLost: 1,
    bleUserJoined: 2,
    bleUserConnected: 3,
    
    sampleMessage: 1000,

	};
	ServerNotification.Type = Type;

	var LocalizedKeys = {
        sampleMessage: "notificationBodyInternal"
	};

	var Status = {
		new : 0,
		read : 1
	};
	ServerNotification.Status = Status;

	//------------------------------------------------------------------------------------------------------------------
	// Internal helpers
	//------------------------------------------------------------------------------------------------------------------

    /**
     * Get message for a specific notification, formatted text
     * @param type {ServerNotification.Type}
     * @return {string: *}
     */
    ServerNotification.internalGetDefaultPayloadType = function(type, localisedParams, extraData){
        if (typeof type == "string")
        {
            type = parseInt(type);
        }
        switch (type)
        {
            case Type.sampleMessage :
                var notif = NotificationManager.generatePayload(settings.labels.applicationTitle,
                    "Fallback message",
                    null,
                    null,
                    LocalizedKeys.sampleMessage,
                    localisedParams,
                    extraData);
                return notif;
                // var notif = NotificationManager.generatePayload(settings.labels.applicationTitle,
                //     "Fallback message",
                //     "notificationBodyAccepted",
                //     null,
                //     null,
                //     null,
                //     extraData);
                // return notif;

        }
    };
  
    /**
     * Send a notifications to all users
     * @param title {string} : title to be sent
     * @param message {string} : message to be sent
     * @return {Promise<function(resolve: ServerNotification, reject: Error)>}
     */

    ServerNotification.internalGenerateServerNotificationUserChangeStatus = function(user, type, message)
    {
      return new Promise(function(resolve, reject){
        var notification = {
          type: type,
          content: "User: " + user.username + message,
          status: Status.new,
          topic: settings.firebase.generalTopic,
          createdAt: Utils.unixtime(),
          updatedAt: Utils.unixtime()
        };
        ServerNotification.upsert(notification).then(function (notification) {
          var additions = {
            notificationType: type,
            user: JSON.stringify(user)
          };
          var data = NotificationManager.generatePayload("BLE Network", notification.content, null, null, null, null, additions);

          NotificationManager.sendToTopic(data, notification.topic, true).then(function(){
            resolve(notification);
          }).catch(function (reason) {
            reject(reason);
          });

        }).catch(function (reason) {
          reject(reason);
        });
      });
    };

    ServerNotification.internalGenerateServerNotificationUpdateUserName = function(user, oldUserName) {
      return new Promise(function(resolve, reject) {
        var notification = {
          type: 9,
          content: "User: " + oldUserName + " changed his name into " + user.username,
          status: Status.new,
          topic: settings.firebase.generalTopic,
          createdAt: Utils.unixtime(),
          updatedAt: Utils.unixtime()
        };
        ServerNotification.upsert(notification).then(function (notification) {
          var additions = {
            notificationType: 9,
            user: JSON.stringify(user)
          };
          var data = NotificationManager.generatePayload("BLE Network", notification.content, null, null, null, null, additions);

          NotificationManager.sendToTopic(data, notification.topic, true).then(function() {
            resolve(notification);
          }).catch(function (reason) {
            reject(reason);
          });

        }).catch(function (reason) {
          reject(reason);
        });
      });
    };

    ServerNotification.internalGenerateServerNotificationNewMessage = function(userMessage, type, message)
    {
      return new Promise(function(resolve, reject){
        var notification = {
          type: type,
          content: message,
          status: Status.new,
          topic: settings.firebase.generalTopic,
          createdAt: Utils.unixtime(),
          updatedAt: Utils.unixtime()
        };
        ServerNotification.upsert(notification).then(function (notification) {
          var additions = {
            notificationType: type,
            messageID: userMessage.id,
            senderID: userMessage.senderID
          };

          var data = NotificationManager.generatePayload("BLE Network", notification.content, null, null, null, null, additions);

          NotificationManager.sendToTopic(data, notification.topic, true).then(function(){
            resolve(notification);
          }).catch(function (reason) {
            reject(reason);
          });

        }).catch(function (reason) {
          reject(reason);
        });
      });
    };

    /**
     * Send a notifications to all users
       * @param title {string} : title to be sent
     * @param message {string} : message to be sent
     * @return {Promise<function(resolve: ServerNotification, reject: Error)>}
     */
    ServerNotification.internalGenerateServerNotification = function(title, message)
    {
      return new Promise(function(resolve, reject){
        var notification = {
          type: Type.internal,
          content: message,
          status: Status.new,
          topic: settings.firebase[loopback.env].generalTopic,
          createdAt: Utils.unixtime(),
          updatedAt: Utils.unixtime()
        };
        ServerNotification.upsert(notification).then(function (notification) {
          var data = NotificationManager.generatePayload(title, message);

          NotificationManager.sendToTopic(data, notification.topic, true).then(function(){
            resolve(notification);
          }).catch(function (reason) {
            reject(reason);
          });

        }).catch(function (reason) {
          reject(reason);
        });
      });
    };

    /**
     * Send a notifications to all users
     * @param type {Type} : message to be sent
     * @param type? {[string]} : message to be sent
     * @return {Promise<function(resolve: ServerNotification, reject: Error)>}
     */
    ServerNotification.internalLocalisedServerNotification = function(type, params, additionalData)
    {
        return new Promise(function(resolve, reject){
            var notification = {
                type: type,
                status: Status.new,
                topic: settings.firebase[loopback.env].generalTopic,
                createdAt: Utils.unixtime(),
                updatedAt: Utils.unixtime()
            };
            if (additionalData)
            {
                if (additionalData.contentID)
                {
                    notification.contentID = additionalData.contentID;
                }
                if (additionalData.userID)
                {
                    notification.userID = additionalData.userID;
                }
                if (additionalData.contentType)
                {
                    notification.contentType = additionalData.contentType;
                }

            }

            ServerNotification.upsert(notification).then(function (notification) {
                var data = ServerNotification.internalGetDefaultPayloadType(type, params, additionalData);

                NotificationManager.sendToTopic(data, notification.topic, true).then(function(){
                    resolve(notification);
                }).catch(function (reason) {
                    reject(reason);
                });

            }).catch(function (reason) {
                reject(reason);
            });
        });
    };


    /**
     * Send a notifications to a user localised text
     * @param user {Clientusers} :date for the notification, requires invitee
     * @return {Promise<function(resolve: ServerNotification, reject: Error)>}
     */
    ServerNotification.internalGenerateNotificationSample = function (user)
    {
      return new promise(function(resolve, reject) {
        if (!user)
        {
          return reject(settings.response.missing_item);
        }
        //generate notification
        var notification = {
          type: Type.sampleMessage,
          contentID: user.id,
          status: Status.new,
          topic: user.topic,
          createdAt: Utils.unixtime(),
          updatedAt: Utils.unixtime(),
          userID: user.id
        };
        ServerNotification.upsert(notification).then(function (notification) {
          var extraData = {
                      userID: user.id,
            notificationType: notification.type

          };

          var data = NotificationManager.generatePayload(settings.labels.applicationTitle,
            null,
                      LocalizedKeys.sampleMessage,
                      [user.email, Utils.formatDate(new Date(), "-")],
            null,
            null,
            extraData);

          NotificationManager.sendToTopic(data, notification.topic, true).then(function() {
            resolve(notification);
          }).catch(function (reason) {
            console.log("internalGenerateNotificationInternal", reason);
            resolve(notification);
          });

        }).catch(function (reason) {
          console.log("internalGenerateNotificationInternal", reason);
          reject(reason);
        });
      });
    };




    //------------------------------------------------------------------------------------------------------------------
    // Admin Remotes
    //------------------------------------------------------------------------------------------------------------------

    ServerNotification.adminSendNotification = function(data, cb)
    {
      if (!data.message)
      {
        cb(null, settings.response.missing_params);
      }
      var title = data.title || settings.labels.applicationTitle;
      ServerNotification.internalGenerateServerNotification(title, data.message).then(function(notification){
        cb(null, settings.response.ok, notification);
      }).catch(function(error){
        cb(error);
      });

      };
    ServerNotification.remoteMethod(
      'adminSendNotification',
      {
        accepts: [
          {arg: 'data', type: 'object', http: {source: 'body'}, description: "{message:{string}, title:{string?}}"}
        ],
        http: {path: '/adminSendNotification', verb: 'post'},
        description: 'Admin: send a notification to all users.',
        returns: [
          {"arg": "error_code", "type": "object"},
          {"arg": "response", "type": "object"}
        ]
      }
    );

    ServerNotification.adminSendPredefinedNotification = function(data, cb)
    {
        if (data.type == undefined)
        {
            cb(null, settings.response.missing_params);
        }

        ServerNotification.internalLocalisedServerNotification(data.type, data.params, data.additionalData).then(function(notification){
            cb(null, settings.response.ok, notification);
        }).catch(function(error){
            cb(error);
        });

    };
    ServerNotification.remoteMethod(
        'adminSendPredefinedNotification',
        {
            accepts: [
                {arg: 'data', type: 'object', http: {source: 'body'}, description: "{type:{int}, params:[string]?, additionalData:{string:*}?}"}
            ],
            http: {path: '/adminSendPredefinedNotification', verb: 'post'},
            description: 'Admin: send a notification to all users.',
            returns: [
                {"arg": "error_code", "type": "object"},
                {"arg": "response", "type": "object"}
            ]
        }
    );

    ServerNotification.adminFindNotifications = function(type, userID,limit, page, cb)
    {
        var filter = {};
        var conditions = [];
        if (type != undefined)
        {
            conditions.push({type: type});
        }
        if (userID != undefined)
        {
            conditions.push({userID: userID});
        }
        if (conditions.length)
        {
            filter.where = {
                and: conditions
            }
        }

        filter.order = "createdAt desc";
        filter.include = ["user"];
        if (limit)
        {
            filter.limit = limit;
            filter.skip = page * limit;
        }

        ServerNotification.find(filter).then(function(notifications){
            cb(null, settings.response.ok, notifications);
        }).catch(function(error){
            cb(error);
        });
    };

    ServerNotification.remoteMethod(
        'adminFindNotifications',
        {
            accepts: [
                {arg: 'type', type: 'number', http: {source: 'query'}, description: "send null for all"},
                {arg: 'userID', type: 'number', http: {source: 'query'}, description: "send null for all"},
                {arg: 'limit', type: 'number', 'http': {source: 'query'}, description: "optional limit, recommended"},
                {arg: 'page', type: 'number', 'http': {source: 'query'}, description: "mandatory if limit is set"}
            ],
            http: {path: '/adminFindNotifications', verb: 'get'},
            description: 'Admin: Find all notifications - filter based on type or user',
            returns: [
                {"arg": "error_code", "type": "object"},
                {"arg": "response", "type": "object"}
            ]
        }
    );


    ServerNotification.adminGetNotification = function(id, cb)
    {
        ServerNotification.findById(id,{include : ["user"]}).then(function(notification){
            if (!notification)
            {
                return cb(null, settings.response.missing_item);
            }
            cb(null, settings.response.ok, notification);
        }).catch(function(error){
            cb(error);
        });
    };
    ServerNotification.remoteMethod(
        'adminGetNotification',
        {
            accepts: [
                {arg: 'id', type: 'number', http: {source: 'query'}, required: true}
            ],
            http: {path: '/adminGetNotification', verb: 'get'},
            description: 'Admin: get a specific transaction',
            returns: [
                {"arg": "error_code", "type": "object"},
                {"arg": "response", "type": "object"}
            ]
        }
    );



};

'use strict';
var loopback = require('loopback');
var ModelManagement = require("../helpers/modelManagement.js");
var promise = require('bluebird');
var path = require('path');
var app = require('../../server/server');
var settings = require('../../settings.json');
var LoopBackContext = require('loopback-context');
var uuidV4 = require('uuid/v4');
var fileExtension = require('file-extension');
var async = require('async');
var Utils = require('../../server/helpers/utils.js');
var AWS = require("aws-sdk");
module.exports = function(AwsStorage) {

    var clientBucket = settings.filesBucket[loopback.env ? loopback.env : "development"].bucket;
    var storageAccessS3 = app.dataSources.storageS3.connector.client.s3;
    var connectorS3 = app.dataSources.storageS3.connector;
    if (app.dataSources.storageS3.settings.useAccelerateEndpoint)
    {
        storageAccessS3.config.update({
            useAccelerateEndpoint: true
        });
    }

    // ModelManagement.disableAllMethods(AwsStorage);

    connectorS3.getFilename = function(file, req, res){
        if (req.params["specificName"])
        {
            return req.params["specificName"] +"."+fileExtension(file.name);
        }
        else if (req.params["prefix"])
        {
            return req.params["prefix"] + "/" + uuidV4() +"."+fileExtension(file.name);
        }
        else{
            return uuidV4() +"."+fileExtension(file.name);
        }

    };
    
    AwsStorage.internalMarkRemoveFile = function(file, bucket)
    {
        var params = {
            Bucket: bucket,
            Key: file,
            Tagging: {
                TagSet: [
                    {
                        Key: 'will_remove',
                        Value: 'delete'
                    }
                ]
            }
        };
        return new promise(function (resolve, reject) {
            storageAccessS3.putObjectTagging(params, function(err, data) {
                if (err) {
                    console.log(err, err.stack); // an error occurred
                    reject(settings.response.general_error);
                }
                else
                {
                    resolve(file);
                }
            });
        });

    };

    AwsStorage.internalMarkRemoveFolder = function(bucket, folder)
    {
        var params = {
            Bucket: bucket, /* required */
            Prefix: folder
        };
        console.log("internalMarkRemoveFolder", folder);
        return new promise(function (resolve, reject) {
            storageAccessS3.listObjects(params, function(err, data) {
                if (err) {
                    console.log(err, err.stack); // an error occurred
                    reject(settings.response.general_error);
                }
                else {
                    console.log("internalMarkRemoveFolder", data);
                    if (data.Contents.length == 0) {
                        return resolve();
                    }

                    var generalError = undefined;
                    var parsedFiles = [];

                    async.eachSeries(data.Contents, function iteratee(file, callback) {
                        if (!generalError)
                        {
                            AwsStorage.internalMarkRemoveFile(file.Key, bucket).then(function(file){
                                parsedFiles.push(file);
                                callback();
                            }).catch(function(error){
                                console.log("internalMarkRemoveFolder", error);
                                callback();
                            });

                        }
                        else
                        {
                            callback();
                        }
                    }, function done() {
                        if (generalError)
                        {
                            reject(generalError);
                        }
                        else {
                            resolve(parsedFiles);
                        }
                    });
                }
            });

        });

    };

    AwsStorage.internalDeleteFolder = function(bucket, folder)
    {
        var params = {
            Bucket: bucket, /* required */
            Prefix: folder
        };

        return new promise(function (resolve, reject) {
            storageAccessS3.listObjects(params, function(err, data) {
                if (err) {
                    console.log(err, err.stack); // an error occurred
                    reject(settings.response.general_error);
                }
                else {

                    if (data.Contents.length == 0) {
                        return resolve();
                    }


                    params = {Bucket: bucket};
                    params.Delete = {Objects: []};

                    data.Contents.forEach(function (content) {
                        params.Delete.Objects.push({Key: content.Key});
                    });

                    storageAccessS3.deleteObjects(params, function (err, data) {
                        if (err) {
                            reject(settings.response.general_error);
                        } else {
                            resolve();
                        }
                    });

                }
            });

        });
    }

    AwsStorage.internalGetFileData = function(bucket, file)
    {
        var params = {
            Bucket: bucket, /* required */
            Key: file
        };

        return new promise(function (resolve, reject) {
            storageAccessS3.getObject(params, function(err, awsFile){
                if (err)
                {
                    console.log(err.stack);
                    return reject(settings.response.general_error);
                }
                else if (awsFile)
                {
                    var data = awsFile.Body;
                    if (data)
                    {
                        resolve(data);
                    }
                    else
                    {
                        console.log("internalGetFileDataData", "No file data found:" + awsFile);
                        return reject(settings.response.missing_files);
                    }
                }
                else
                {
                    console.log("internalGetFileDataData", "No file found:" + awsFile);
                    return reject(settings.response.missing_files);
                }
            });
        });
    }

    AwsStorage.internalGetFolderContent = function(bucket, folder) {
        var params = {
            Bucket: bucket, /* required */
            Prefix: folder
        };

        return new promise(function (resolve, reject) {
            storageAccessS3.listObjects(params, function(err, data) {
                if (err) {
                    console.log(err, err.stack); // an error occurred
                    reject(settings.response.general_error);
                }
                else {

                    if (data.Contents.length == 0) {
                        return resolve([]);
                    }

                    return resolve(data.Contents);
                }
            });

        });
    };

    AwsStorage.internalGetFileSize = function(bucket, file)
    {
        var params = {
            Bucket: bucket, /* required */
            Key: file
        };

        return new promise(function (resolve, reject) {
            storageAccessS3.getObject(params, function(err, awsFile){
                if (err)
                {
                    console.log(err.stack);
                    return reject(settings.response.general_error);
                }
                else if (awsFile)
                {
                    var contentLength = awsFile.ContentLength;
                    if (contentLength)
                    {
                        resolve(contentLength);
                    }
                    else
                    {
                        console.log("internalGetFileSize", "No file data found:" + awsFile);
                        return reject(settings.response.missing_files);
                    }
                }
                else
                {
                    console.log("internalGetFileSize", "No file found:" + awsFile);
                    return reject(settings.response.missing_files);
                }
            });
        });
    };

    AwsStorage.internalGetFolderSize = function(bucket, folder) {
        var params = {
            Bucket: bucket, /* required */
            Prefix: folder
        };

        return new promise(function (resolve, reject) {
            storageAccessS3.listObjects(params, function(err, data) {
                if (err) {
                    console.log(err, err.stack); // an error occurred
                    reject(settings.response.general_error);
                }
                else {

                    if (data.Contents.length == 0) {
                        return resolve(0);
                    }

                    var contentLength = 0;
                    data.Contents.forEach(function (content) {
                        contentLength += content.Size;
                    });

                    return resolve(contentLength);
                }
            });

        });
    };

    AwsStorage.internalUploadBuffer = function(bucket, fileName, buffer)
    {
        return new promise(function (resolve, reject) {
            // var base64data = new Buffer(buffer, 'binary');

            storageAccessS3.putObject({
                Bucket: bucket,
                Key: fileName,
                Body: buffer
            }, function(error, data){
                if (error)
                {
                    console.log(error.stack);
                    reject(settings.response.general_error);
                }
                else
                {
                    resolve(fileName);

                }
            });
        });
    };


    /**
     * Generate presigned URL to access private resource file
     * @param bucket
     * @param file
     * @param ttl
     */
    AwsStorage.internalGenerateSignedGetURL = function(bucket, file, ttl)
    {
        if (!ttl)
        {
            ttl = 3600;
        }

        return new promise(function (resolve, reject) {
            storageAccessS3.getSignedUrl('getObject', {
                Bucket: bucket,
                Key: file,
                Expires: ttl
            }, function (error, value) {
                if (error)
                {
                    console.log("AwsStorage.generateSignedGetURL", error);
                    reject(settings.response.missing_item);
                }
                else
                {
                    //TODO-SETUP update url
                    //sample
                    // var URL = require('url');
                    // var fileURL = URL.parse(value);
                    // if (bucket == settings.filesBucket[loopback.env].bucket)
                    // {
                    //     resolve(settings.filesBucket[loopback.env].url +fileURL.path);
                    // }
                    resolve(value);
                }
            });

        });
    };


    AwsStorage.internalGenerateSignedPutURL = function (bucket,type,key, ttl) {
        return new promise(function (resolve, reject) {
            try {
                if (!ttl)
                {
                    ttl = 300;
                }

                var url = storageAccessS3.getSignedUrl('putObject', {
                    Bucket: bucket,
                    Key: key,
                    Expires: ttl,
                    ContentType: type
                });

                resolve(url);

            }
            catch (e) {
                console.log("internalGenerateSignedPutURL", e);
                reject(settings.response.general_error);
            }

        });
    };

    AwsStorage.internalFileMetadata = function(bucket, fileName)
    {
        return new promise(function(resolve, reject){
            var params = {
                Bucket: bucket,
                Key: fileName
            };
            storageAccessS3.headObject(params, function (error, metadata) {
                if (error)
                {
                    reject(settings.response.missing_files);
                }
                else
                {
                    resolve(metadata);
                }
            });

        });
    }
    /**
     * Returns promise - on resolve the read stream for the file
     * @param bucket - for the file
     * @param fileName - file name(includes folders)
     */
    AwsStorage.internalStreamFile = function(bucket, fileName)
    {
        return new promise(function(resolve, reject){
            var params = {
                Bucket: bucket,
                Key: fileName
            };
            var file = storageAccessS3.getObject(params);
            if (!file)
            {
                reject(settings.response.missing_files);
            }
            else
            {
                var stream = file.createReadStream();
                resolve(stream);
            }

        });
    }

};

'use strict';
var loopback = require('loopback'),
    LoopBackContext = require('loopback-context'),
    promise = require('bluebird'),
    async = require('async');
var Utils = require("../helpers/utils.js");
var ModelManagement = require('../helpers/modelManagement.js');

/**
 * Model for application parameters that change during application life.
 * EX: system parameters, like lastLambdaRun
 * EX: app parametes, settable from admin panel
 * @param ApplicationParams
 */
module.exports = function(ApplicationParams) {

    //names of the parameters
    ApplicationParams.paramNames = {
        reportLimitIntervalDays : "reportLimitIntervalDays",
        reportMaxAllowed: "reportMaxAllowed"
    };

    var settings = require('../../settings.json');
    var app = require('../../server/server');
    ModelManagement.disableAllMethods(ApplicationParams);

    //------------------------------------------------------------------------------------------------------------------
    // Internal helpers
    //------------------------------------------------------------------------------------------------------------------


    /**
     * Get parameters, must not be system and must not be expired
     * @return {Promise<function(resolve: [ApplicationParams], reject: Error)>}
     */
    ApplicationParams.internalGetParameters = function()
    {
        return new promise(function(resolve, reject){
            var filter = {
                where: {
                    and : [
                        {expiredAt: null},
                        {system :0}
                    ]
                }
            };

            ApplicationParams.find(filter).then(function(parameters){
                resolve(parameters);
            }).catch(function(error){
                console.log(error.stack);
                reject(settings.response.db_error);
            });
        });
    };

    /**
     * Get parameters, must not be system and must not be expired. Will be returned as a map
     * @return {Promise<function(resolve: {string : *}, reject: Error)>}
     */
    ApplicationParams.internalGetParametersValues = function()
    {
        return new promise(function(resolve, reject){
            ApplicationParams.internalGetParameters().then(function (prices) {
                var values = {};
                for (var index = 0; index < prices.length; index++)
                {
                    var price = prices[index];
                    values[price.paramName] = price.paramValue;
                }
                resolve(values);
            }).catch(function(error){
                reject(error);
            });
        });
    };

    /**
     * Get a specific parameter, ony current version is considered. If the parameter doesn't exist it is created with the specified value.
     * will convert db type to paramType
     * Note: the param will be set as system
     * @param paramName {string} : name of the parameter
     * @param defaultValue {*} : default value, in case the param doesn't exist
     * @return {Promise<function(resolve: *, reject: Error)>}
     */
    ApplicationParams.internalGetParameter = function(paramName, defaultValue)
    {
        return new promise(function (resolve, reject) {
            var filter = {};
            filter.where = {paramName : paramName, expiredAt: null};
            filter.limit = 1;
            ApplicationParams.find(filter).then(function(params) {
                if (params.length == 0)
                {
                    var param = {
                        paramName: paramName,
                        paramValue : defaultValue,
                        createdAt: new Date().getTime() / 1000,
                        version : 0
                    };
                    if (typeof defaultValue == "number")
                    {
                        param.valueType = "number"
                    }
                    else
                    {
                        param.valueType = "string";
                    }
                    ApplicationParams.upsert(param).then(function(param){
                        resolve(param.paramValue);

                    }).catch(function (reason) {
                        console.log(reason.stack);
                        reject(settings.response.db_error);
                    });
                }
                else
                {
                    if (params[0].valueType == "number")
                    {
                        return resolve(Number(params[0].paramValue));
                    }
                    resolve(params[0].paramValue);
                }
            }).catch(function (reason) {
                console.log(reason.stack);
                reject(settings.response.db_error);
            });
        });
    };

    /**
     * Update the value for a parameter - can create new version. If param doesn't exist it is created as a non-system param
     * @param paramName {string} : name of the parameter
     * @param value {*} : default value, in case the param doesn't exist
     * @param createVersion {int} : 0 for false, 1 for true
     * @param description {string} : text description for the param
     * @return {Promise<function(resolve: ApplicationParams, reject: Error)>}
     */
    ApplicationParams.internalUpdateParameter = function(paramName, value, createVersion, description)
    {
        return new promise(function (resolve, reject) {
            var filter = {};
            filter.where = {paramName : paramName, expiredAt: null};
            filter.limit = 1;
            ApplicationParams.find(filter).then(function(params) {
                var param = undefined;
                var prevParam = undefined;
                if (params.length > 0)
                {
                    if (createVersion)
                    {
                        prevParam = params[0];
                        prevParam.expiredAt = Utils.unixtime();
                        param = {
                            paramName: paramName,
                            paramValue : value,
                            createdAt: Utils.unixtime(),
                            version : prevParam.version  + 1,
                            system : prevParam.system,
                            description : description ? description : prevParam.description,
                            displayType : prevParam.displayType,
                            valueType : prevParam.valueType
                        };
                    }
                    else
                    {
                        param = params[0];
                        param.paramValue = value;
                        if (description)
                        {
                            param.description = description;
                        }

                    }
                }
                else
                {
                    param = {
                        paramName: paramName,
                        paramValue : value,
                        createdAt: Utils.unixtime(),
                        system : 0,
                        version : 0,
                        description : description
                    };
                }
                ApplicationParams.upsert(param).then(function(param){
                    if (prevParam)
                    {
                        ApplicationParams.upsert(prevParam).then(function(){
                            resolve(param);
                        }).catch(function (reason) {
                            console.log(reason.stack);
                            reject(settings.response.db_error);
                        });
                    }
                    else
                    {
                        resolve(param);
                    }
                }).catch(function (reason) {
                    console.log(reason.stack);
                    reject(settings.response.db_error);
                });

            }).catch(function (reason) {
                console.log(reason.stack);
                reject(settings.response.db_error);
            });
        });
    };

    ApplicationParams.internalUpdateSystemParameter = function(paramName, value)
    {
        return new promise(function (resolve, reject) {
            var filter = {};
            filter.where = {paramName : paramName, expiredAt: null};
            filter.limit = 1;
            ApplicationParams.find(filter).then(function(params) {
                var param = undefined;
                if (params.length > 0)
                {
                    param = params[0];
                    param.paramValue = value;
                }
                else
                {
                    param = {
                        paramName: paramName,
                        paramValue : value,
                        createdAt: Utils.unixtime(),
                        system : 1,
                        version : 0,
                        description : "System:" + paramName
                    };
                }
                ApplicationParams.upsert(param).then(function(param){
                    resolve(param);
                }).catch(function (reason) {
                    console.log(reason.stack);
                    reject(settings.response.db_error);
                });

            }).catch(function (reason) {
                console.log(reason.stack);
                reject(settings.response.db_error);
            });
        });
    };

    //------------------------------------------------------------------------------------------------------------------
    // Admin - Remote methods
    //------------------------------------------------------------------------------------------------------------------

    ModelManagement.restrictAccessAdmin(ApplicationParams, ["adminSetParameters", "adminGetParameters"]);

    ApplicationParams.adminGetParameters = function (name, currentVersion, cb) {
        var filter = {};
        filter.where = {};
        if (name)
        {
            filter.where.paramName = name;
        }
        if (currentVersion == 1)
        {
            filter.where.expiredAt = null
        }
        filter.where.system = 0;
        filter.order = ["paramName ASC","createdAt DESC"];
        ApplicationParams.find(filter).then(function (params) {
            cb(null, settings.response.ok, params);
        }).catch(function(error){
            console.log("ApplicationParams", error.stack);
            cb(null, settings.response.db_error);
        });
    };

    ApplicationParams.remoteMethod(
        'adminGetParameters',
        {
            accepts: [{arg: 'name', type: 'string', http: {source: 'query'}, description: "Name of a specific parameter"},
                {arg: 'currentVersion', type: 'number', http: {source: 'query'}, description: "set to 1 if only current version should be returned"}],
            http: {path: '/adminGetParameters', verb: 'get'},
            description: 'Application: get parameters',
            returns: [
                {'arg': 'error_code', 'type': 'object'},
                {'arg': 'response', 'type': 'object'}
            ]
        }
    );

    ApplicationParams.adminSetParameters = function (data, cb) {
        if (!data.paramName)
        {
            return cb(null, settings.response.missing_params);
        }
        var createVersion = data.createVersion ? 1 : 0;

        ApplicationParams.internalUpdateParameter(data.paramName, data.paramValue, createVersion, data.description).then(function(param){
            cb(null, settings.response.ok, param);
        }).catch(function(error){
            cb(null, error);
        });
    };

    ApplicationParams.remoteMethod(
        'adminSetParameters',
        {
            accepts: [{arg: 'data', type: 'object', http: {source: 'body'}, description: "{paramName:{string}, paramValue:{string},createVersion: {0/1}?, description: {string}}"}],
            http: {path: '/adminSetParameters', verb: 'post'},
            description: 'Application: set a parameter',
            returns: [
                {'arg': 'error_code', 'type': 'object'},
                {'arg': 'response', 'type': 'object'}
            ]
        }
    );
};

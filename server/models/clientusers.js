'use strict';
var loopback = require('loopback');
var promise = require('bluebird');
var path = require('path');
var LoopBackContext = require('loopback-context');
var uuidV4 = require('uuid/v4');
var ModelManagement = require("../helpers/modelManagement.js");
var Utils = require('../../server/helpers/utils.js');
var async = require('async');

module.exports = function (Clientusers) {
    var self = this;
    var app = require('../../server/server');
    var settings = require('../../settings.json');

    var serverUrl = settings["server_url"][loopback.env];
    var _ = require('lodash');
    var cron = require('cron-scheduler');
    var checkUsersJob = cron({on: '* * * * *'}, function () {
      console.log("Running task", new Date());
      if (Clientusers.clientVerifyIfIsAlive)
      {
        Clientusers.clientVerifyIfIsAlive(function (err, error_code) {
          console.log("completed - cron", err, error_code);
        });
      }
    });
    // checkUsersJob.run()

    Clientusers.settings.resetPasswordTokenTTL = 24 * 60 * 60;
    Clientusers.ttl = 5 * 60 * 60;
    //add other roles that are valid from mobile
    //used to filter user results
    Clientusers.moblieUserRoles = ["business"];
    Clientusers.Status = {
      connecting : 0,
      connected : 1,
      warning : 2,
      lost : 3,
      disconnected : 4
    };

    var filesBucket = settings.filesBucket[loopback.env];
  
    var NotificationType = {
      bleUserJoined: 1,
      bleUserConnected: 2,
      bleUserWarning: 3,
      bleUserLost: 4,
      bleUserDisconnected: 5
    };
  
    var NotificationMessage = {
      bleUserMessageLost: " has been lost",
      bleUserMessageJoined: " has joined in the group",
      bleUserMessageConnected: " has connected",
      bleUserMessageDisconnected: " has disconnected",
      bleUserMessageWarning: " warning",
    };

    ModelManagement.disableAllMethods(Clientusers, ['logout', 'confirm', 'resetPassword']);
    ModelManagement.responseWrapper(Clientusers);


    //----------------------------------------------------------------------
    // validations
    //---------------------------------------------------------------------

    /**
     * Check if the user is an admin
     * @param user
     * @return {boolean}
     */
    Clientusers.isAdmin = function(user)
    {
        return (user.role == "admin");
    };

    /**
     * Internal validate password rules
     * @param password {string}
     * @return {boolean} - true if the password is valid, false otherwise
     */
    Clientusers.internalValidatePassword = function(password)
    {
        if (password.length < Clientusers.passwordRules.min) return false;
        else if (password.length > Clientusers.passwordRules.max) return false;
        return true;
    };


    //apisettings
    Clientusers.apiSettings = function(cb)
    {
        var ApplicationParams = app.models.ApplicationParams;
        ApplicationParams.internalGetParametersValues().then(function(response) {
            response.fileStorage = filesBucket;
            response.generalTopic = settings.firebase[loopback.env].generalTopic;
            response.environment = loopback.env;
            response.passwordRules = Clientusers.passwordRules;
            cb(null, settings.response.ok, response);
        }).catch(function(err){
            cb(err);
        });

    };
    Clientusers.remoteMethod(
        'apiSettings',
        {
            http: {path: '/apiSettings', verb: 'get'},
            description: 'Mobile application: API settings',
            returns: [
                {"arg": "error_code", "type": "object"},
                {"arg": "response", "type": "object"}
            ]
        }
    );
  
    //------------------------------------------------------------------------------------------------------------------
    //methods for internal update
    //------------------------------------------------------------------------------------------------------------------

    Clientusers.internalRemoveNeighbour = function(deviceID, users)
    {
      return new promise(function(resolve, reject) {
        if(users.length == 0)
        {
          return resolve();
        }
        var userErrors = [];
        async.eachSeries(users, function(user, callback) {
          var userNeighbours = user.neighbours ? user.neighbours.split(",") : [];
          var myEdges = []
          for (var index1 = 0; index1 < userNeighbours.length; index1++)
          {
            if (userNeighbours[index1] != deviceID)
            {
              myEdges.push(userNeighbours[index1]);
            }
          }
          user.neighbours = myEdges.join(",");
          if (user.neighbours.length == 0)
          {
            let oldStatus = user.status
            user.status = Clientusers.Status.lost;
            user.lastUpdate = Utils.unixtime();
            if(user.status != oldStatus)
            {
              var ServerNotification = app.models.ServerNotification;
              ServerNotification.internalGenerateServerNotificationUserChangeStatus(user.toJSON(), NotificationType.bleUserLost, NotificationMessage.bleUserMessageLost).then(function () {

              }).catch(function () {

              }).finally(function () {

              })
            }
          }
          Clientusers.upsert(user).then(function () {
            callback();
          }).catch(function (err) {
            console.log(err);
            userErrors.push(err);
            callback();
          });

        }, function(){
          if(userErrors.length)
          {
            return reject(settings.response.db_error);
          }
          else {
            return resolve();
          }

        });


      });
    }

    Clientusers.internalAddNeighbour = function(deviceID, users)
    {
      return new promise(function(resolve, reject) {
        if(users.length == 0)
        {
          return resolve();
        }
        else
        {
          var userErrors = [];
          async.eachSeries(users, function(user, callback) {
            var userNeighbours = user.neighbours ? user.neighbours.split(",") : [];
            var myEdges = [];
            for (var index1 = 0; index1 < userNeighbours.length; index1++)
            {
              if (userNeighbours[index1] != deviceID)
              {
                myEdges.push(userNeighbours[index1]);
              }
            }
            if (myEdges.length == userNeighbours.length)
            {
              myEdges.push(deviceID);
              user.neighbours = myEdges;
              var oldStatus = user.status
              user.status = Clientusers.Status.connected;
              user.lastUpdate = Utils.unixtime();
              Clientusers.upsert(user).then(function () {
                if(oldStatus != user.status)
                {
                  var ServerNotification = app.models.ServerNotification;
                  ServerNotification.internalGenerateServerNotificationUserChangeStatus(user.toJSON(), NotificationType.bleUserConnected, NotificationMessage.bleUserMessageConnected).then(function () {

                  }).catch(function () {

                  }).finally(function () {
                    callback();
                  })
                }
              }).catch(function (err) {
                console.log(err);
                userErrors.push(err)
                callback();
              });
            }
          }, function() {
            if(userErrors.length)
            {
              return reject(settings.response.db_error);
            }
            else
              {
              return resolve();
            }
          });
        }
      })
    }

    Clientusers.internalUpdateStatusNeighbour = function(status, users)
    {
      return new promise(function (resolve, reject) {
        if(users.length == 0)
        {
          return resolve();
        }
        else
        {
          var userErrors = [];
          async.eachSeries(users, function(user, callback) {
            var oldStatus = user.status;
            user.status = status
            Clientusers.upsert(user).then(function () {
              if(oldStatus != user.status)
              {
                var ServerNotification = app.models.ServerNotification;
                if(user.status == Clientusers.Status.warning)
                {
                  ServerNotification.internalGenerateServerNotificationUserChangeStatus(user.toJSON(), NotificationType.bleUserWarning, NotificationMessage.bleUserMessageWarning).then(function () {

                  }).catch(function() {

                  }).finally(function() {
                    callback();
                  })
                }
                else if(user.status == Clientusers.lost)
                {
                  ServerNotification.internalGenerateServerNotificationUserChangeStatus(user.toJSON(), NotificationType.bleUserLost, NotificationMessage.bleUserMessageLost).then(function () {

                  }).catch(function() {

                  }).finally(function() {
                    callback();
                  })
                }
                else
                {
                  callback();
                }
              }
            }).catch(function (err) {
              console.log(err);
              userErrors.push(err)
              callback();
            });
          }, function () {
            if(userErrors.length)
            {
              return reject(settings.response.db_error);
            }
            else
            {
              return resolve();
            }
          });
        }
      })
    }

    //------------------------------------------------------------------------------------------------------------------
    //methods for login
    //------------------------------------------------------------------------------------------------------------------

    /**
     * Wrap response for login - used for username/email login as well as facebook login
     * @param accessToken
     * @param user
     * @return {*}
     */
    Clientusers.internalLoginResponse = function (accessToken, user)
    {
        var responseWrapper = accessToken;
        responseWrapper.user = user;
        responseWrapper.bluetoothID = settings.bluetoothID;
        responseWrapper.firebaseTopic = settings.firebase.generalTopic;
        return responseWrapper;
    };

    Clientusers.clientLogin = function (data, cb)
    {
        data.ttl = 5 * 60 * 60; //5*60min
        if (!(data.username && data.deviceID))
        {
            cb(null, settings.response.missing_params);
            return;
        }
        var filter = {};
        filter.where = {"username": data.username};
        Clientusers.findOne(filter).then(function (user1) {
          if (!user1 || (user1 != undefined && user1.deviceID == data.deviceID))
          {
            filter.where = {"deviceID": data.deviceID};
            Clientusers.findOne(filter).then(function (user) {
              if (!user)
              {
                user = {
                  deviceID: data.deviceID,
                  username: data.username,
                  topic: Utils.generateTopic("Firebase"),
                  password: "placeholder",
                  status: Clientusers.Status.connecting
                }
              }
              else
              {
                user.username = data.username;
                user.status = Clientusers.Status.connecting;
              }
              
              Clientusers.upsert(user).then(function (user2) {
                user2.createAccessToken(data.ttl).then(function (token) {
              
                  var response = Clientusers.internalLoginResponse(token, user2);
                  var ServerNotification = app.models.ServerNotification;
                  ServerNotification.internalGenerateServerNotificationUserChangeStatus(user2.toJSON(), NotificationType.bleUserJoined, NotificationMessage.bleUserMessageJoined).then(function () {
    
                  }).catch(function () {
    
                  }).finally(function () {
                    cb(null, settings.response.ok, response);
                  })
                }).catch(function (err) {
                  console.log("clientLogin", err);
                  cb(null, settings.response.db_error);
                });
              }).catch(function (err) {
                console.log("clientLogin", err);
                cb(null, settings.response.db_error);
              });
            }).catch(function (err) {
              console.log("clientLogin", err);
              cb(null, settings.response.db_error);
            });
          }
          else
          {
            cb(null, settings.response.username_exists);
          }
        }).catch(function (err) {
          console.log("clientLogin", err);
          cb(null, settings.response.db_error);
        });
    };

    Clientusers.remoteMethod(
        'clientLogin',
        {
            accepts: {arg: 'data', type: 'object', http: {source: 'body'}},
            http: {path: '/clientLogin', verb: 'post'},
            description: 'Mobile application: Login with email/username and password',
            returns: [
                {"arg": "error_code", "type": "object"},
                {"arg": "response", "type": "object"}
            ]
        }
    );


    Clientusers.clientLogout = function (data, cb)
    {
        var ctx = LoopBackContext.getCurrentContext();
        var currentUser = ctx && ctx.get('currentUser');
        var token = ctx && ctx.get('currentToken');
        if (!currentUser)
        {
            var error = new Error();
            error.statusCode = 401;
            error.message = 'Authorization Required';
            cb(error);
            return;
        }
        Clientusers.logout(token.id)
            .then(function () {
               currentUser.status = Clientusers.Status.disconnected;
               var neighbours = currentUser.neighbours ? currentUser.neighbours.split(",") : [];
               var filterNeighbours = {
                 where : {
                   deviceID: {inq : neighbours},
                   status : {neq: Clientusers.Status.disconnected}
                 }
               };
               Clientusers.find(filterNeighbours).then(function (users) {
                 Clientusers.internalRemoveNeighbour(currentUser.deviceID, users).then(function () {
                   currentUser.neighbours = "";
                   currentUser.latitude = null;
                   currentUser.longitude = null;
                   Clientusers.upsert(currentUser).then(function () {
                     var ServerNotification = app.models.ServerNotification;
                     ServerNotification.internalGenerateServerNotificationUserChangeStatus(currentUser.toJSON(), NotificationType.bleUserDisconnected, NotificationMessage.bleUserMessageDisconnected).then(function () {

                     }).catch(function () {

                     }).finally(function () {
                       cb(null, settings.response.ok);
                     })
                   }).catch(function (err) {
                     console.log(err);
                     cb(null, settings.response.ok);
                   });
                 }).catch(function (err) {
                   console.log(err);
                   cb(null, settings.response.ok);
                 });
               }).catch(function (err) {
                 console.log(err);
                 cb(null, settings.response.ok);
               });
            })
            .catch(function (err) {
                console.log(err);
                cb(null, settings.response.db_error);
            });
    };
    
    Clientusers.remoteMethod(
        'clientLogout',
        {
            accepts: {arg: 'data', type: 'object', http: {source: 'body'}},
            http: {path: '/clientLogout', verb: 'post'},
            description: 'Mobile application: Logout',
            returns: [
                {"arg": "error_code", "type": "object"}
            ]
        }
    );

    Clientusers.clientVerifyIfIsAlive = function (cb)
    {
        var filterWarningUsers = {
          where : { and : [
              {lastUpdate: {lte: Utils.unixtime() - 60}},
              {status: {neq: Clientusers.Status.disconnected}},
              {status: {neq: Clientusers.Status.warning}},
              {status: {neq: Clientusers.Status.lost}},
              {lastUpdate: {gt: Utils.unixtime() - 300}}
          ]}
        };
        var filterLostUsers = {
          where : { and : [
              {lastUpdate: {lte: Utils.unixtime() - 300}},
              {status: {neq: Clientusers.Status.disconnected}},
              {status: {neq: Clientusers.Status.lost}}
            ]}
        };
        Clientusers.find(filterWarningUsers).then(function (users) {
          Clientusers.internalUpdateStatusNeighbour(Clientusers.Status.warning, users).then(function () {
            Clientusers.find(filterLostUsers).then(function (users1) {
              Clientusers.internalUpdateStatusNeighbour(Clientusers.Status.lost, users1).then(function () {
                cb(null, settings.response.ok);
              }).catch(function (err) {
                console.log(err);
                cb(null, settings.response.db_error);
              });
            }).catch(function (err) {
              console.log(err);
              cb(null, settings.response.db_error);
            });
          }).catch(function (err) {
            console.log(err);
            cb(null, settings.response.db_error);
          });
        }).catch(function (err) {
          console.log(err);
          cb(null, settings.response.db_error);
        });
    };

    Clientusers.remoteMethod(
      'clientVerifyIfIsAlive',
      {
        http: {path: '/clientVerifyIfIsAlive', verb: 'get'},
        description: 'Mobile application: verify if an user is alive',
        returns: [
          {"arg": "error_code", "type": "object"}
        ]
      }
    );
  
    Clientusers.clientUpdateNeighbours = function (data, cb)
    {
        if (data.neighbours == undefined)
        {
          cb(null, settings.response.missing_params);
          return;
        }
        var ctx = LoopBackContext.getCurrentContext();
        var currentUser = ctx && ctx.get('currentUser');
        if (!currentUser)
        {
          var error = new Error();
          error.statusCode = 401;
          error.message = 'Authorization Required';
          cb(error);
          return;
        }
        var prevNeighbours = currentUser.neighbours ? currentUser.neighbours.split(",") : [];
        var latitude = data.latitude;
        var longitude = data.longitude;
        var currentNeighbours = data.neighbours ? data.neighbours.split(",") : [];
        var filterNeighbours = {
          where : {
            deviceID: {inq : currentNeighbours},
            status : {neq: Clientusers.Status.disconnected}
          }
        };
        Clientusers.find(filterNeighbours).then(function(users) {
          var currentActiveNeighbours = [];
          var deleteFromNeighbourEdge = [];
          var addNeighbourEdge = [];
          for (var index = 0; index < users.length; index++)
          {
            currentActiveNeighbours.push(users[index].deviceID);
          }
          for (var index = 0; index < prevNeighbours.length; index++)
          {
            var found = false;
            for (var index1 = 0; index1 < currentActiveNeighbours.length; index1++)
            {
              if (prevNeighbours[index] == currentActiveNeighbours[index1])
              {
                found = true;
                break;
              }
            }
            if (found === false)
            {
              deleteFromNeighbourEdge.push(prevNeighbours[index]);
            }
          }
          for (var index1 = 0; index1 < currentActiveNeighbours.length; index1++)
          {
            var found = false;
            for (var index = 0; index < prevNeighbours.length; index++)
            {
              if (prevNeighbours[index] == currentActiveNeighbours[index1])
              {
                found = true;
                break;
              }
            }
            if (found === false)
            {
              addNeighbourEdge.push(currentActiveNeighbours[index1]);
            }
          }
          var filterDeleteNeighbours = {
            where : {
              deviceID: {inq : deleteFromNeighbourEdge},
              status : {neq: Clientusers.Status.disconnected}
            }
          };
          Clientusers.find(filterDeleteNeighbours).then(function (usersDelete) {
            Clientusers.internalRemoveNeighbour(currentUser.deviceID, usersDelete).then(function () {
              var filterAddNeighbours = {
                where : {
                  deviceID: {inq : addNeighbourEdge},
                  status : {neq: Clientusers.Status.disconnected}
                }
              };
              Clientusers.find(filterAddNeighbours).then(function (usersAdd) {
                Clientusers.internalAddNeighbour(currentUser.deviceID, usersAdd).then(function () {
                  var oldStatus = currentUser.status
                  if (currentActiveNeighbours.length == 0)
                  {
                    currentUser.status = Clientusers.Status.lost;
                  }
                  else
                  {
                    currentUser.status = Clientusers.Status.connected;
                  }
                  currentUser.neighbours = currentActiveNeighbours.join(",");
                  currentUser.lastUpdate = Utils.unixtime();
                  currentUser.latitude = latitude ? latitude : null;
                  currentUser.longitude = longitude? longitude : null;

                  Clientusers.upsert(currentUser).then(function () {
                    if(currentUser.status == Clientusers.Status.lost && currentUser.status != oldStatus)
                    {
                      var ServerNotification = app.models.ServerNotification;
                      ServerNotification.internalGenerateServerNotificationUserChangeStatus(currentUser.toJSON(), NotificationType.bleUserLost, NotificationMessage.bleUserMessageLost).then(function () {

                      }).catch(function () {

                      }).finally(function () {
                        cb(null, settings.response.ok);
                      })
                    }
                    else if(currentUser.status != oldStatus)
                    {
                      var ServerNotification = app.models.ServerNotification;
                      ServerNotification.internalGenerateServerNotificationUserChangeStatus(currentUser.toJSON(), NotificationType.bleUserConnected, NotificationMessage.bleUserMessageConnected).then(function () {

                      }).catch(function () {

                      }).finally(function () {
                        cb(null, settings.response.ok);
                      })
                    }
                    else
                    {
                      cb(null, settings.response.ok);
                    }
                  }).catch(function (err) {
                    console.log(err);
                    cb(null, settings.response.db_error);
                  });
                }).catch(function (err) {
                  console.log(err);
                  cb(null, settings.response.db_error);
                });
              }).catch(function (err) {
                console.log(err);
                cb(null, settings.response.db_error);
              });
            }).catch(function (err) {
              cb(null, err);
            });
          }).catch(function (err) {
            console.log(err);
            cb(null, settings.response.db_error);
          });
        }).catch(function (err) {
          console.log(err);
          cb(null, settings.response.db_error);
        });
    };
    
    Clientusers.remoteMethod(
      'clientUpdateNeighbours',
      {
        accepts: {arg: 'data', type: 'object', http: {source: 'body'}},
        http: {path: '/clientUpdateNeighbours', verb: 'post'},
        description: 'Mobile application: Update device status',
        returns: [
          {"arg": "error_code", "type": "object"},
          {"arg": "response", "type": "object"}
        ]
      }
    );

    Clientusers.clientUpdateUserName = function(data, cb)
    {
      if(!data.username) {
        cb(null, settings.response.missing_params);
        return;
      }
      var ctx = LoopBackContext.getCurrentContext();
      var currentUser = ctx && ctx.get('currentUser');
      var currentToken = ctx && ctx.get('currentToken');
      if (!currentUser)
      {
        var error = new Error();
        error.statusCode = 401;
        error.message = 'Authorization Required';
        cb(error);
        return;
      }
      let oldUserName = currentUser.username;
      currentUser.username = data.username;
      currentUser.isValid(function (valid) {
        if (!valid)
        {
          return cb(null, settings.response.username_exists);
        }
        Clientusers.upsert(currentUser).then(function (user) {
          app.models.AccessToken.upsert(currentToken).then(function () {
            var ServerNotification = app.models.ServerNotification;
            ServerNotification.internalGenerateServerNotificationUpdateUserName(currentUser.toJSON(), oldUserName).then(function () {

            }).catch(function () {

            }).finally(function () {
              cb(null, settings.response.ok, currentUser);
            })
          }).catch(function (err) {
            console.log("clientUpdateUserName", err);
            cb(null, settings.response.db_error);
          });
        }).catch(function (err) {
          console.log("clientUpdateUserName", err);
          cb(null, settings.response.db_error);
        });
      });
    };

    Clientusers.remoteMethod(
        'clientUpdateUserName',
        {
          accepts: {arg: 'data', type: 'object', http: {source: 'body'}},
          http: {path: '/clientUpdateUserName', verb: 'post'},
          description: 'Mobile application: Update client username',
          returns: [
            {"arg": "error_code", "type": "object"},
            {"arg": "response", "type": "object"}
          ]
        }
    );
  
    Clientusers.adminLogin = function (data, cb)
    {
        data.ttl = 5 * 60 * 60; //5*60min
        if (!((data.username || data.email) && data.password))
        {
            cb(null, settings.response.missing_params);
            return;
        }
        var filter = {};
        if (data.username)
        {
            filter.where = {"username": data.username};
        }
        else if (data.email)
        {
            filter.where = {"email": data.email};
        }
        else
        {
            cb(null, settings.response.missing_params);
        }
        Clientusers.find(filter)
            .then(function (users) {
                if (users.length == 0)
                {
                    var error = new Error();
                    error.statusCode = 401;
                    error.message = settings.response.user_not_found.message;
                    cb(error, settings.response.user_not_found);
                    return;
                }
                var user = users[0];
                if (!Clientusers.isAdmin(user))
                {
                    var error = new Error();
                    error.statusCode = 401;
                    error.message = settings.response.user_not_found.message;
                    cb(error, settings.response.user_not_found);
                    return;
                }
                Clientusers.login(data)
                    .then(function (result) {
                        var rez = result.toJSON();
                        Clientusers.findById(result.userId)
                            .then(function (user) {
                                var response = Clientusers.internalLoginResponse(rez, user);
                                cb(null, settings.response.ok, response);
                            }).catch(function (err) {
                            console.log("adminLogin", err);
                            var error = new Error();
                            error.statusCode = 401;
                            error.message = settings.response.db_error.message;
                            cb(error, settings.response.db_error);
                        });
                    }).catch(function (err) {
                    console.log("adminLogin", err);
                    var error = new Error();
                    error.statusCode = 401;
                    error.message = settings.response.login_error.message;
                    cb(error, settings.response.login_error);

                })
            }).catch(function (err) {
            console.log("adminLogin", err);
            var error = new Error();
            error.statusCode = 401;
            error.message = settings.response.db_error.message;
            cb(error, settings.response.db_error);
        });

    };

    Clientusers.remoteMethod(
        'adminLogin',
        {
            accepts: {arg: 'data', type: 'object', http: {source: 'body'}},
            http: {path: '/adminLogin', verb: 'post'},
            description: 'Dashboard application: Login',
            returns: [
                {"arg": "error_code", "type": "object"},
                {"arg": "response", "type": "object"}
            ]
        }
    );

    //------------------------------------------------------------------------------------------------------------------
    //register account
    //------------------------------------------------------------------------------------------------------------------


    /**
     * Internal generate an admin
     * @param email
     * @param password
     */
    Clientusers.internalAdminRegister = function(email, password)
    {
        return new promise(function (resolve, reject) {
            Clientusers.findOne({where:{email:email}}).then(function(admin){
                if (!admin)
                {
                    var data = {
                        email: email,
                        password : password,
                        role: "admin",
                        createdAt: Utils.unixtime(),
                        updatedAt: Utils.unixtime(),
                        username: email,
                        topic: Utils.generateTopic("Admin"),
                        storageFolder: uuidV4()
                    };

                    Clientusers.upsert(data).then(function (user) {
                        resolve(user);
                    }).catch(function (err) {
                        console.log("Clientusers.internalAdminRegister ", err.stack);
                        reject(settings.response.db_error);
                    });

                }
                else
                {
                    resolve(admin);
                }
            }).catch(function (error) {
                reject(error);
            });

        });
    };
    
    //------------------------------------------------------------------------------------------------------------------
    // Other users
    //------------------------------------------------------------------------------------------------------------------
    Clientusers.findUsers = function(keyword, order, limit, page, cb)
    {
        var ctx = LoopBackContext.getCurrentContext();
        var currentUser = ctx && ctx.get('currentUser');

        if(!currentUser)
        {
            var error = new Error();
            error.statusCode = 401;
            error.message ='Authorization Required';
            cb(error);
            return;
        }

        var filter = {};
        if (keyword)
        {
            var keywordLike = "%" + keyword +"%";
	        filter.where = {and : [
			        {or :[
					        {username:{like: keywordLike}},
					        {deviceID:{like: keywordLike}}
				        ]
			        },
			        {role :{
			        	inq:Clientusers.moblieUserRoles}
			        }
		        ]};
        }
        else
        {
	        filter.where = {role:{inq:Clientusers.moblieUserRoles}};
        }
        if (limit)
        {
            filter.limit = limit;
            filter.skip = page * limit;
        }
        if (order)
        {
            filter.order = order.split(",");
        }

        Clientusers.find(filter).then(function(users){
            for(var index = 0; index < users.length; index++) {
              users[index].bluetoothID = settings.bluetoothID;
              users[index].firebaseTopic = settings.firebase.generalTopic;
            }
            cb(null, settings.response.ok, users);
        }).catch(function(error){
            console.log("Clientusers.findUsers", error);
            cb(null, settings.response.db_error);
        });

    };
    Clientusers.remoteMethod(
        'findUsers',
        {
            accepts:  [
                {arg: 'keyword', type: 'string', 'http': {source: 'query'}},
                {arg: 'order', type: 'string', 'http': {source: 'query'}, description: "order : list of columns comma separated - uses split ex: "},
                {arg: 'limit', type: 'number', 'http': {source: 'query'}},
                {arg: 'page', type: 'number', 'http': {source: 'query'}}
            ],
            http: {path: '/findUsers', verb: 'get'},
            description:'Mobile application: Find users based on keyword',
            returns:   [
                {"arg": "error_code","type": "object"  },
                {"arg": "response",  "type": "object"    }
            ]
        }
    );

    Clientusers.getUser = function(userID, cb) {
        var ctx = LoopBackContext.getCurrentContext();
        var currentUser = ctx && ctx.get('currentUser');

        if(!currentUser)
        {
            var error = new Error();
            error.statusCode = 401;
            error.message ='Authorization Required';
            cb(error);
            return;
        }

        if (!userID)
        {
            return cb(null, settings.response.missing_params);
        }
        Clientusers.findById(userID).then(function(user){
            if (!user)
            {
                return cb(null, settings.response.missing_item);
            }

            cb(null, settings.response.ok, user);

        }).catch(function(error){
            console.log("Clientusers.getUser", error);
            cb(null, settings.response.db_error);
        });

    };
    
    Clientusers.remoteMethod(
        'getUser',
        {
            accepts:  [
                {arg: 'userID', type: 'number', 'http': {source: 'query'}}
            ],
            http: {path: '/getUser', verb: 'get'},
            description:'Mobile application: Get user details',
            returns:   [
                {"arg": "error_code","type": "object"  },
                {"arg": "response",  "type": "object"    }
            ]
        }
    );

};

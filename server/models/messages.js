'use strict';
var loopback = require('loopback');
var promise = require('bluebird');
var path = require('path');
var LoopBackContext = require('loopback-context');
var uuidV4 = require('uuid/v4');
var ModelManagement = require("../helpers/modelManagement.js");
var Utils = require('../../server/helpers/utils.js');
var async = require('async');

module.exports = function(Messages) {

  ModelManagement.disableAllMethods(Messages);

  var self = this;
  var app = require('../../server/server');
  var settings = require('../../settings.json');

  Messages.Type = {
    text: 1,
    image: 2,
    coordinates: 3
  };

  var NotificationType = {
    text: 6,
    image: 7,
    coordinates: 8
  }

  var NotificationMessage = {
    text: " sended a new message",
    image: " sended an image",
    coordinates: " sended location"
  }

  Messages.createMessage = function(data, cb)
  {
    var ctx = LoopBackContext.getCurrentContext();
    var currentUser = ctx && ctx.get('currentUser');
    if (!currentUser)
    {
      var error = new Error();
      error.statusCode = 401;
      error.message = 'Authorization Required';
      cb(error);
      return;
    }
    if (!data.content)
    {
      return cb(null, settings.response.missing_params);
    }
    var message = {
      type: Messages.Type.text,
      content: data.content,
      createdAt: Utils.unixtime(),
      senderID: currentUser.deviceID
    };
    Messages.upsert(message).then(function (message) {
      var ServerNotification = app.models.ServerNotification;
      var username = currentUser.username
      ServerNotification.internalGenerateServerNotificationNewMessage(message.toJSON(), NotificationType.text, username + NotificationMessage.text).then(function () {

      }).catch(function () {
        console.log("eroare")
      }).finally(function () {
        cb(null, settings.response.ok, message);
      })
    }).catch(function (error) {
      console.log("Messages.createMessage", error);
      cb(null, settings.response.db_error);
    });
  };

  Messages.remoteMethod("createMessage" ,
    {
      accepts: {arg: 'data', type: 'object', http: {source: 'body'}},
      http: {path: '/createMessage', verb: 'post'},
      description: 'Mobile application: send a new chat message',
      returns: [
        {"arg": "error_code", "type": "object"},
        {"arg": "response", "type": "object"}
      ]
    });

  Messages.createCoordinatesMessage = function(req, res, cb)
  {
    var ctx = LoopBackContext.getCurrentContext();
    var currentUser = ctx && ctx.get('currentUser');
    if (!currentUser)
    {
      var error = new Error();
      error.statusCode = 401;
      error.message = 'Authorization Required';
      cb(error);
      return;
    }
    var clientBucket = settings.filesBucket[loopback.env].bucket;
    var AwsStorage = app.models.AwsStorage;
    req.params.prefix = "ChatImagesLocations";
    AwsStorage.upload(req,res, {container: clientBucket}, function(err,response){
      if (err)
      {
        console.log("createCoordinatesMessage", err);
        return cb(null, settings.response.general_error);
      }
      else if(!response.files || !response.files.file) {
        console.log(response);
        cb(null, settings.response.missing_files);
      }
      else
      {
        var data = response.fields;
        if(!(data.storageData && Array.isArray(data.storageData)))
        {
          AwsStorage.removeFile(clientBucket, response.files.file[0].name,function(completed, error)
          {
            return  cb(null, settings.response.missing_params);
          });
          return;
        }
        data = data.storageData[0];
        if (typeof data=='string')
          data=JSON.parse(data);
        var locationContent = data.content + "," + response.files.file[0].name;
        var message = {
          type: Messages.Type.coordinates,
          senderID: currentUser.deviceID,
          createdAt: Utils.unixtime(),
          content: locationContent,
          aspectRatio : 1
        };
        Messages.upsert(message).then(function (message) {
          var ServerNotification = app.models.ServerNotification;
          var username = currentUser.username;
          ServerNotification.internalGenerateServerNotificationNewMessage(message.toJSON(), NotificationType.coordinates, username + NotificationMessage.coordinates).then(function () {

          }).catch(function () {
            console.log("eroare")
          }).finally(function () {
            cb(null, settings.response.ok, message);
          });
        }).catch(function (error) {
          console.log("Messages.createMessage", error);
          cb(null, settings.response.db_error);
        });
      }
    });
  };

  Messages.remoteMethod("createCoordinatesMessage" ,
    {
      accepts: [
        {arg: 'req', type: 'object', http: {source: 'req'}},
        {arg: 'res', type: 'object', http: {source: 'res'}}
      ],
      http: {path: '/createCoordinatesMessage', verb: 'post'},
      description: 'Mobile application: send a new chat coordinates message',
      returns: [
        {"arg": "error_code", "type": "object"},
        {"arg": "response", "type": "object"}
      ]
    });


  Messages.createImageMessage = function(req, res, cb)
  {
    var ctx = LoopBackContext.getCurrentContext();
    var currentUser = ctx && ctx.get('currentUser');
    if (!currentUser)
    {
      var error = new Error();
      error.statusCode = 401;
      error.message = 'Authorization Required';
      cb(error);
      return;
    }
    var clientBucket = settings.filesBucket[loopback.env].bucket;
    var AwsStorage = app.models.AwsStorage;
    req.params.prefix = "ChatImages";
    AwsStorage.upload(req,res, {container: clientBucket}, function(err,response){
      if (err)
      {
        console.log("createImageMessage", err);
        return cb(null, settings.response.general_error);
      }
      else if(!response.files || !response.files.file) {
        console.log(response);
        cb(null, settings.response.missing_files);
      }
      else
      {
        var data = response.fields;
        if(!(data.storageData && Array.isArray(data.storageData)))
        {
          AwsStorage.removeFile(clientBucket, response.files.file[0].name,function(completed, error)
          {
            return  cb(null, settings.response.missing_params);
          });
          return;
        }
        data = data.storageData[0];
        if (typeof data=='string')
          data=JSON.parse(data);
        var message = {
          type: Messages.Type.image,
          senderID: currentUser.deviceID,
          createdAt: Utils.unixtime(),
          content: response.files.file[0].name,
        };
        if (data.aspectRatio)
        {
          message.aspectRatio = data.aspectRatio;
        }
        Messages.upsert(message).then(function (message) {
          var ServerNotification = app.models.ServerNotification;
          var username = currentUser.username;
          ServerNotification.internalGenerateServerNotificationNewMessage(message.toJSON(), NotificationType.image, username + NotificationMessage.image).then(function () {

          }).catch(function () {
            console.log("eroare")
          }).finally(function () {
            cb(null, settings.response.ok, message);
          });
        }).catch(function (error) {
          console.log("Messages.createMessage", error);
          cb(null, settings.response.db_error);
        });
      }
    });
  };

  Messages.remoteMethod("createImageMessage" ,
    {
      accepts: [
        {arg: 'req', type: 'object', http: {source: 'req'}},
        {arg: 'res', type: 'object', http: {source: 'res'}}
      ],
      http: {path: '/createImageMessage', verb: 'post'},
      description: 'Mobile application: send a new chat message with an image',
      returns: [
        {"arg": "error_code", "type": "object"},
        {"arg": "response", "type": "object"}
      ]
    });


  Messages.getMessages = function(createdAt, limit, page, cb)
  {
    var ctx = LoopBackContext.getCurrentContext();
    var currentUser = ctx && ctx.get('currentUser');

    if (!currentUser)
    {
      var error = new Error();
      error.statusCode = 401;
      error.message = 'Authorization Required';
      cb(error);
      return;
    }

    var filter = {
      where : {
        createdAt : {gt: createdAt}
      },
      include: ["sender"],
      order: ["createdAt ASC"]
    };

    if (limit)
    {
      filter.limit = limit;
      if(page)
      {
        filter.skip = limit * page;
      }
    }

    Messages.find(filter).then(function (messages) {
      cb(null, settings.response.ok, messages);
    }).catch(function (error) {
      console.log("Messages.getMessages", error);
      cb(null, settings.response.db_error);
    });
  };

  Messages.remoteMethod("getMessages" ,
    {
      accepts: [
        {arg: 'createdAt', type: 'number', http: {source: 'query'}, description: "unixtime in seconds", required: true},
        {arg: 'limit', type: 'number', http: {source: 'query'}, description: "pagination"},
        {arg: 'page', type: 'number', http: {source: 'query'}, description: "pagination"}
        ],
      http: {path: '/getMessages', verb: 'get'},
      description: 'Mobile application: get messages after a specific date',
      returns: [
        {"arg": "error_code", "type": "object"},
        {"arg": "response", "type": "object"}
      ]
    });

  Messages.getMessage = function (id, cb)
  {
    var ctx = LoopBackContext.getCurrentContext();
    var currentUser = ctx && ctx.get('currentUser');

    if (!currentUser)
    {
      var error = new Error();
      error.statusCode = 401;
      error.message = 'Authorization Required';
      cb(error);
      return;
    }

    var filter = {
      include: ["sender"]
    };

    Messages.findById(id, filter).then(function (message) {
      if (!message)
      {
        return cb(null, settings.response.missing_item);
      }

      cb(null, settings.response.ok, message);
    }).catch(function (error) {
      console.log("Messages.getMessage", error);
      cb(null, settings.response.db_error);
    });
  };

  Messages.remoteMethod("getMessage" ,
    {
      accepts: [
        {arg: 'id', type: 'number', http: {source: 'query'}, description: "id of the message", required: true}
      ],
      http: {path: '/getMessage', verb: 'get'},
      description: 'Mobile application: get a specific message',
      returns: [
        {"arg": "error_code", "type": "object"},
        {"arg": "response", "type": "object"}
      ]
    });

};

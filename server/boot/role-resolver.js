module.exports = function(app) {
    var Role = app.models.Role;

    Role.registerResolver('admin', function(role, context, cb) {
        function reject() {
            process.nextTick(function() {
                cb(null, false);
            });
        }

        // do not allow anonymous users
        var userId = context.accessToken.userId;
        if (!userId) {
            return reject();
        }
        app.models.Clientusers.findById(userId, function(err, user) {
            if (err) {
                return reject();
            }
            if (!user) {
                return reject();
            }
            //console.log(user);
            if(!user.role) return reject();
            else if(user.role=='admin') cb(null,true);
            else cb(null, false);
        });
    });

    Role.registerResolver('business', function(role, context, cb) {
        function reject() {
            process.nextTick(function() {
                cb(null, false);
            });
        }

        // do not allow anonymous users
        var userId = context.accessToken.userId;
        if (!userId) {
            return reject();
        }
        app.models.Clientusers.findById(userId, function(err, user) {

            if (err) {
                return reject();
            }
            if (!user) {
                return reject();
            }
            //console.log(user);
            if(!user.role) return reject();
            else if(user.role=='business') cb(null,true);
            else cb(null, false);
        });
    });
};

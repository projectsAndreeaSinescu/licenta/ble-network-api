var nodemailer = require("nodemailer");
var AWS = require('aws-sdk');
var SES = new AWS.SES();
var S3 = new AWS.S3();

//requires SES(read, write, list) permissions and SQS(sendEmail, sendRawEmail) permissions

function getFiles(filesDescription)
{
    return new Promise(function (resolve, reject) {
        if (filesDescription.length == 0)
        {
            return resolve([]);
        }
        var file = filesDescription[0].file;
        var filename = filesDescription[0].name;

        var bucket = file.substr(0, file.indexOf("/"));
        var key = file.substr(file.indexOf("/") + 1);
        if (!bucket || !key)
        {
            return reject("Failed to find file:"+file);
        }

        S3.getObject(
            {
                Bucket: bucket,
                Key: key
            },
            function (err, data) {
                if (err) {
                    console.log(bucket, key, err);
                    return reject("Failed to get file:" + file)
                }
                else
                {
                    var fileData = {
                        filename: filename,
                        content: data.Body
                    };

                    getFiles(filesDescription.slice(1), filesDescription.length).then(function(files){
                        files.push(fileData);
                        resolve(files);
                    }).catch(function (error) {
                        reject(error);
                    });
                }
            }
        );
    });

}

exports.handler = function (event, context, callback)
{
    // console.log(JSON.stringify(event));

    if (event.Records && event.Records.length ==0 )
    {
        return callback(null, "No emails to send");
    }
    else
    {
        var message = event.Records[0];
        var toList = undefined;
        try {
            toList = JSON.parse(message.messageAttributes.to.stringValue);
        }
        catch (e) {
            toList = [message.messageAttributes.to.stringValue]
        }

        var mailOptions = {
            from: message.messageAttributes.from.stringValue,
            subject: message.messageAttributes.subject.stringValue,
            to: toList,
            // bcc: Any BCC address you want here in an array,
        };


        var params = {
            Destination :{
                ToAddresses: toList
            },
            Message : {
                Body :{

                },
                Subject : {
                    Charset: "UTF-8",
                    Data: message.messageAttributes.subject.stringValue
                }
            },
            Source : message.messageAttributes.from.stringValue
        }

        var retry = 0;
        if (message.messageAttributes.retryCounter)
        {
            try
            {
                retry = Number(message.messageAttributes.retryCounter.stringValue);
            }
            catch(e)
            {

            }
        }

        if (message.messageAttributes.bcc)
        {
            try {
                mailOptions.bcc = JSON.parse(message.messageAttributes.bcc.stringValue);
            }
            catch (e) {
                mailOptions.bcc = [message.messageAttributes.bcc.stringValue]
            }
        }

        if (message.messageAttributes.cc)
        {
            try {
                mailOptions.cc = JSON.parse(message.messageAttributes.cc.stringValue);
            }
            catch (e) {
                mailOptions.cc = [message.messageAttributes.cc.stringValue]
            }

        }

        if (message.messageAttributes.html)
        {
            mailOptions.html = message.messageAttributes.html.stringValue
        }
        if (message.messageAttributes.body)
        {
            mailOptions.html = message.messageAttributes.body.stringValue
        }

        var attachments = [];

        if (message.messageAttributes.attachments)
        {
            try {
                attachments = JSON.parse(message.messageAttributes.attachments.stringValue);
            }
            catch (e) {
                console.log("Invalid Attachments", e);
            }
        }

        // create Nodemailer SES transporter
        var transporter = nodemailer.createTransport({
            SES: SES
        });

        if (attachments.length == 0)
        {
            // send email
            transporter.sendMail(mailOptions, function (err, info) {
                if (err)
                {
                    console.log(err, err.stack); // an error occurred
                    if (retry)
                    {
                        callback("Failed to send but may retry Subject:"+ params.Subject.Data + " to" + JSON.stringify(toList));
                    }
                    else
                    {
                        callback(null, "Failed (0 retry) sent email Subject:"+ params.Subject.Data + " to" + JSON.stringify(toList));
                    }
                }
                else
                {
                    console.log(info); // an error occurred
                    callback(null, "Sent email "+ params.Message.Subject.Data + " to" + JSON.stringify(toList));
                }
            });

        }
        else
        {
            //get attachments
            getFiles(attachments).then(function (attachments) {
                console.log("attachments", attachments);
                mailOptions.attachments = attachments;
                transporter.sendMail(mailOptions, function (err, info) {
                    if (err)
                    {
                        console.log(err, err.stack); // an error occurred
                        if (retry)
                        {
                            callback("Failed to send but may retry Subject:"+ mailOptions.subject + " to " + JSON.stringify(toList));
                        }
                        else
                        {
                            callback(null, "Failed (0 retry) sent email Subject:"+ mailOptions.subject + " to " + JSON.stringify(toList));
                        }
                    }
                    else
                    {
                        console.log(info); // an error occurred
                        callback(null, "Sent email "+ mailOptions.subject + " to " + JSON.stringify(toList));
                    }
                });
            }).catch(function(err){
                console.log(err);
                if (retry)
                {
                    callback("Failed to send attachments but may retry Subject:"+ mailOptions.subject + " to " + JSON.stringify(toList));
                }
                else
                {
                    callback(null, "Failed (0 retry) send attachments  email Subject:"+ mailOptions.subject + " to " + JSON.stringify(toList));
                }
            });
        }

    }

};

var https = require('https');

//TODO: Setup - Configure with application specific
var emailSenders = [
    {
        from: ["startup-dev@hypersense.ro"],
        host: "dev.api.startup.com"
    },
    {
        from: ["startup@hypersense.ro"],
        host: "api.startup.com"
    }];
var URI = '/api/v1/EmailProcessings/systemComplaintEmails';

function sendToAPI(host, messages)
{
    return new Promise(function (resolve, reject) {
        if (messages.length == 0)
        {
            return resolve();
        }

        var post_options = {
            host: host,
            path: URI,
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            }
        };
        var post_request = https.request(post_options, function(res) {
            var body = '';

            res.on('data', function(chunk)  {
                body += chunk;
            });

            res.on('end', function() {

                console.log(body);
                if (typeof body == 'string')
                {
                    body = JSON.parse(body);
                }
                if (!body.error_code)
                {
                    return reject(-100);
                }
                var errorCode = body.error_code.code;
                if (errorCode != 0)
                {
                    console.log("ERROR - ", host, JSON.stringify(body));
                    reject(errorCode);
                }
                else
                {
                    resolve();
                }

            });

            res.on('error', function(e) {
                console.log("ERROR - ",e.stack);
                resolve();
            });
        });

        post_request.write(JSON.stringify({messages:messages}), "utf8");

        post_request.on('error', function(e) {
            console.log("ERROR - ",e.stack);
            reject(-100);
        });

        post_request.end();
    });

}

function sendToAPIRecursive(senders, messages, index)
{
    return new Promise(function (resolve, reject) {
        if (messages.length == 0)
        {
            return resolve();
        }
        if (index >= senders.length)
        {
            return resolve();
        }
        sendToAPI(senders[index].host, messages[index]).then(function () {
            sendToAPIRecursive(senders, messages, index+1).then(function () {
                resolve();
            }).catch(function (e) {
                reject(e);
            })
        }).catch(function (e) {
            reject(e);
        });

    });
}

exports.handler = function(events,context)
{

    var records = events.Records;
    if (!records.length)
    {
        return context.succeed();
    }

    //split messages
    var messages = [];
    for (var typeIndex = 0; typeIndex < emailSenders.length; typeIndex++)
    {
        messages.push([]);
    }
    for (var recordIndex = 0; recordIndex < records.length; recordIndex++)
    {
        var message = records[recordIndex].Message;
        if (records[recordIndex].body)
        {
            if (typeof records[recordIndex].body == "string")
            {
                try {
                    message = JSON.parse(records[recordIndex].body);
                }
                catch (e) {
                    console.log(e);
                }
            }
            message = message.Message;
        }
        if (typeof message == "string")
        {
            try {
                message = JSON.parse(message);
            }
            catch (e) {
                console.log(e);
            }
        }

        if (message && message.mail)
        {
            for (var typeIndex = 0; typeIndex < emailSenders.length; typeIndex++)
            {
                if (emailSenders[typeIndex].from.indexOf(message.mail.source) != -1)
                {
                    messages[typeIndex].push(message)
                }
            }

        }

    }

    sendToAPIRecursive(emailSenders, messages, 0).then(function () {
        return context.succeed();
    }).catch(function (e) {
        //caught error
        console.log("Error - but success", e);
        return context.succeed();
    });

};


